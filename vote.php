<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


session_start();

$page = 'vote';
require_once('database.php');
require_once('header.php');
function get_web_page( $url )
    {
        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h4>Vote for your favorite Coins to be added to the exchange</h4>
            </div>
            <div class="col s12"><p>Initial coin votes will continue until the exchange has been launched. Top 3 voted coins will be listed on the exchange initially. After the exchange has launched top coin of the month will be added. All coins used to vote will be sent to the specified coins burn address. Prices to list coins on the exchange will be posted when exchange launches.</p></div>
            <div class="col s12"><p><b>****** IMPORTANT - To vote send 1 wiki for every vote to the burn address of the coin you want to vote for below ******</b></p></div>			
			<div class="col s12">
				<table class="highlight responsive-table">
					<thead>
						<tr>
							<th>Rank</th>
							<th></th>
							<th>Symbol</th>
							<th>Name</th>
							<th>Website</th>
							<th>Voting Burn Address</th>
							<th># of Votes</th>
						</tr>
					</thead>
					<tbody>
<?php
						$records = array();
						$i = 0;
						$result = $mysqli->query("SELECT symbol, name, logo, url, allowtxid FROM coins where allowreward = '1'");
						if($result) 
						{
							while($row = $result->fetch_object()) 
							{
								$allowtxid = $row->allowtxid;
								$results = get_web_page('http://explorer.altcoinwiki.io:3001/ext/getbalance/'.$allowtxid.'');
								$records[$i] = array('symbol'=>$row->symbol, 'name'=>$row->name, 'logo'=>$row->logo, 'website'=>$row->url, 'allowtxid'=>$row->allowtxid, 'votes'=>$results['content']);

								if ( $results['errno'] != 0 ){ echo "... error: bad url, timeout, redirect loop ..."; }
								if ( $results['http_code'] != 200 ){ echo "... error: no page, no permissions, no service ..."; }
								$i++;
							}
							foreach ($records as $key => $row) {
								$votes[$key]  = $row['votes'];
							}
							array_multisort($votes, SORT_DESC, $records);
							$a = 1;
							foreach ($records as $value) 
							{

								$logoin = $value['logo'];
								$symbolin = $value['symbol'];
								$namein = $value['name'];
								$websitein = $value['website'];
								$allowtxid = $value['allowtxid'];
								$votesin = $value['votes'];
								$num = $a;
								echo '<tr><td>'.$num.'</td><td><img src=images\coins\\'.$logoin.' alt= height="24" width="24"></td><td>'.$symbolin.'</td><td>'.$namein.'</td><td>'.$websitein.'</td><td>'.$allowtxid.'</td><td>'.$votesin.'</td></tr>';
								$a++;
							}
						}
?>						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>