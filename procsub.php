<?php
require_once('database.php');
@session_start();

//require_once('database.php');
if(isset($_SESSION) && isset($_SESSION['loggedIn'])) {
    $seed = $_SESSION['loggedIn'];
    $query = "SELECT * FROM users WHERE guid = '$seed'";
	$query1 = "SELECT * FROM wiki_admin WHERE user_guid = '$seed'";
    if($res = $mysqli->query($query)) {
        if($u = $res->fetch_object()) {
            $user = $u;
        }
    }
    if($res1 = $mysqli->query($query1)) {
        if($u1 = $res1->fetch_object()) {
            $admin = $u1;
        }
    }
    unset($seed);
}

if(isset($user) && !isset($admin)) {
	if(isset($_POST) && isset($_POST['terms'])) {
		if($_POST['terms'] == 'on'){
			$selected_val = $_POST['TierSel'];
			$FeatSel = $_POST['feat'];
			if($selected_val == '0'){
				$guid = $_SESSION['loggedIn'];
				$projname = $_POST['projname'];
				$ticker = $_POST['ticker'];
				$website = $_POST['website'];
				$specsalgo = $_POST['specsalgo'];
				$specspow = $_POST['specspow'];
				$logo = $ticker . $_FILES['myfile']['name'];
				$txid = $_POST['txid'];
				

				
				if (!preg_match("/^[a-zA-Z ]*$/",$projname)) {
					$nameErr = "Only letters and white space allowed"; 
				}
				if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
					$websiteErr = "Invalid URL"; 
				}
				$result0 = $mysqli->query("SELECT * FROM coins where symbol = '".$ticker."'");
				if($result0->num_rows == 0){
					//echo "INSERT INTO `coins`(user_guid, name, symbol, url, specsalgo, specspow, logo, approved) VALUES ('$guid','$projname','$ticker','$website', '$specsalgo', '$specspow', '$logo','0')";
					$query = "INSERT INTO `coins`(user_guid, name, symbol, url, specsalgo, specspow, logo, approved) VALUES ('$guid','$projname','$ticker','$website', '$specsalgo', '$specspow', '$logo','0')";
					$res = $mysqli->query($query);
					$query1 = "INSERT INTO `package`(approvedp, name, package, expire, txid, user_guid) VALUES ('0','$ticker','0','2999-01-01','','$guid')";
					$res1 = $mysqli->query($query1);
					if ($FeatSel == 0){
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','2017-01-01','2017-01-01','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}elseif ($FeatSel == 1){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}else if ($FeatSel == 2){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','2','$txid','0')";
						$res2 = $mysqli->query($query2);					
					}else if ($FeatSel == 3){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','3','$txid','0')";
						$res2 = $mysqli->query($query2);
					}					
					$currentDir = getcwd();
					$uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/images/uploadsaltwiki324fs8ses3wefsF/';

					$errors = []; // Store all foreseen and unforseen errors here

					$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
					$fileExtension = '';
					$fileName = $_FILES['myfile']['name'];
					$fileSize = $_FILES['myfile']['size'];
					$fileTmpName  = $_FILES['myfile']['tmp_name'];
					$fileType = $_FILES['myfile']['type'];
					$tmp = explode('.', $fileName);
					$fileExtension= end($tmp);
					$fileExtension = strtolower($fileExtension);
					$uploadPath = $uploadDirectory . $ticker . basename($fileName);
					
					if (! in_array($fileExtension,$fileExtensions)) {
						$errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
					}

					if ($fileSize > 2000000) {
						$errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
					}

					if (empty($errors)) {
						$didUpload = move_uploaded_file($fileTmpName, $uploadPath);

						if ($didUpload) {
							//echo "The file " . basename($fileName) . " has been uploaded";
						} else {
							echo "<script type='text/javascript'>alert('An error occurred somewhere. Try again or contact the admin');</script>";
						}
					} else {
						foreach ($errors as $error) {
							echo "<script type='text/javascript'>alert('. $error .');</script>";
							echo $error . "These are the errors" . "\n";
						}
					}
				}
			}

			else if($selected_val == '1'){
				$guid = $_SESSION['loggedIn'];
				$projname = $_POST['projname'];
				$ticker = $_POST['ticker'];
				$website = $_POST['website'];
				$logo = $ticker . $_FILES['myfile']['name'];
				$githublink = $_POST['githublink'];
				$btctalklink = $_POST['btctalklink'];
				$facebooklink = $_POST['facebooklink'];
				$twitterlink = $_POST['twitterlink'];
				$discordlink = $_POST['discordlink'];
				$telegramlink = $_POST['telegramlink'];
				$specsalgo = $_POST['specsalgo'];
				$specspow = $_POST['specspow'];				
				$txid = $_POST['txid'];
				$date = date("Y-m-d", strtotime(" +1 months"));
				
				$result0 = $mysqli->query("SELECT * FROM coins where symbol = '".$ticker."'");
				if($result0->num_rows == 0){
					$query = "INSERT INTO `coins`(user_guid, name, symbol, url, logo, approved, github, btctalk, facebook, twitter, discord, telegram, specsalgo, specspow, txid) VALUES ('$guid','$projname','$ticker','$website','$logo','0', '$githublink', '$btctalklink', '$facebooklink', '$twitterlink', '$discordlink', '$telegramlink', '$specsalgo', '$specspow', '$txid')";
					$res = $mysqli->query($query);
					$query1 = "INSERT INTO `package`(approvedp, name, package, expire, txid, user_guid) VALUES ('0','$ticker','1','$date','','$guid')";
					$res1 = $mysqli->query($query1);
					if ($FeatSel == 0){
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','2017-01-01','2017-01-01','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}elseif ($FeatSel == 1){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}else if ($FeatSel == 2){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','2','$txid','0')";
						$res2 = $mysqli->query($query2);					
					}else if ($FeatSel == 3){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','3','$txid','0')";
						$res2 = $mysqli->query($query2);
					}	
				
					//$currentDir = getcwd();
					//$uploadDirectory = "\images\\";
					$currentDir = getcwd();
					$uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/images/uploadsaltwiki324fs8ses3wefsF/';

					$errors = []; // Store all foreseen and unforseen errors here

					$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
					$fileExtension = '';
					$fileName = $_FILES['myfile']['name'];
					$fileSize = $_FILES['myfile']['size'];
					$fileTmpName  = $_FILES['myfile']['tmp_name'];
					$fileType = $_FILES['myfile']['type'];
					$tmp = explode('.', $fileName);
					$fileExtension= end($tmp);
					$fileExtension = strtolower($fileExtension);
					$uploadPath = $uploadDirectory . $ticker . basename($fileName);
					
					if (! in_array($fileExtension,$fileExtensions)) {
						$errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
					}

					if ($fileSize > 2000000) {
						$errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
					}

					if (empty($errors)) {
						$didUpload = move_uploaded_file($fileTmpName, $uploadPath);

						if ($didUpload) {
							//echo "The file " . basename($fileName) . " has been uploaded";
						} else {
							echo "<script type='text/javascript'>alert('An error occurred somewhere. Try again or contact the admin');</script>";
						}
					} else {
						foreach ($errors as $error) {
							echo "<script type='text/javascript'>alert('. $error .');</script>";
							echo $error . "These are the errors" . "\n";
						}
					}
				}
			}

			else if($selected_val == '2'){
				$guid = $_SESSION['loggedIn'];
				$projname = $_POST['projname'];
				$ticker = $_POST['ticker'];
				$website = $_POST['website'];
				$logo = $ticker . $_FILES['myfile']['name'];
				$githublink = $_POST['githublink'];
				$btctalklink = $_POST['btctalklink'];
				$facebooklink = $_POST['facebooklink'];
				$twitterlink = $_POST['twitterlink'];
				$discordlink = $_POST['discordlink'];
				$telegramlink = $_POST['telegramlink'];
				$description = $_POST['description'];
				$specsalgo = $_POST['specsalgo'];
				$specspow = $_POST['specspow'];
				$totalcoinsissued = $_POST['totalcoinsissued'];
				$blocktime = $_POST['blocktime'];
				$blockreward = $_POST['blockreward'];
				$blockmature = $_POST['blockmature'];
				$masternodeonlinelink = $_POST['masternodeonlinelink'];
				$masternodecolatt = $_POST['masternodecolatt'];
				$maternodereward = $_POST['maternodereward'];
				$explorerlink = $_POST['explorerlink'];
				$txid = $_POST['txid'];
				$date = date("Y-m-d", strtotime(" +1 months"));
				
				$result0 = $mysqli->query("SELECT * FROM coins where symbol = '".$ticker."'");
				if($result0->num_rows == 0){
					$query = "INSERT INTO `coins`(user_guid, name, symbol, url, logo, approved, github, btctalk, facebook, twitter, discord, telegram, description, specsalgo, specspow, totalcoinsissued, blocktime, blockreward, blockmature, mno, masternodecolatt, maternodereward, explorer, txid) VALUES ('$guid','$projname','$ticker','$website','$logo','0', '$githublink', '$btctalklink', '$facebooklink', '$twitterlink', '$discordlink', '$telegramlink', '$description', '$specsalgo', '$specspow', '$totalcoinsissued', '$blocktime', '$blockreward', '$blockmature', '$masternodeonlinelink', '$masternodecolatt', '$maternodereward', '$explorerlink', '$txid')";	
//echo "INSERT INTO `coins`(user_guid, name, symbol, url, logo, approved, github, btctalk, facebook, twitter, discord, telegram, description, specsalgo, specspow, totalcoinsissued, blocktime, blockreward, blockmature, mno, masternodecolatt, maternodereward, explorer, txid) VALUES ('$guid','$projname','$ticker','$website','$logo','0', '$githublink', '$btctalklink', '$facebooklink', '$twitterlink', '$discordlink', '$telegramlink', '$description', '$specsalgo', '$specspow', '$totalcoinsissued', '$blocktime', '$blockreward', '$blockmature', '$masternodeonlinelink', '$masternodecolatt', '$maternodereward', '$explorerlink', '$txid')";					
					$res = $mysqli->query($query);
					$query1 = "INSERT INTO `package`(approvedp, name, package, expire, txid, user_guid) VALUES ('0','$ticker','2','$date','$txid','$guid')";
					$res1 = $mysqli->query($query1);
					if ($FeatSel == 0){
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','2017-01-01','2017-01-01','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}elseif ($FeatSel == 1){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','1','$txid','0')";
						//echo "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}else if ($FeatSel == 2){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','2','$txid','0')";
						//echo "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','2','$txid','0')";
						$res2 = $mysqli->query($query2);					
					}else if ($FeatSel == 3){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','3','$txid','0')";
						//echo "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','3','$txid','0')";
						$res2 = $mysqli->query($query2);
					}	
				
					$currentDir = getcwd();
					$uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/images/uploadsaltwiki324fs8ses3wefsF/';

					$errors = []; // Store all foreseen and unforseen errors here

					$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
					$fileExtension = '';
					$fileName = $_FILES['myfile']['name'];
					$fileSize = $_FILES['myfile']['size'];
					$fileTmpName  = $_FILES['myfile']['tmp_name'];
					$fileType = $_FILES['myfile']['type'];
					$tmp = explode('.', $fileName);
					$fileExtension= end($tmp);
					$fileExtension = strtolower($fileExtension);
					$uploadPath = $uploadDirectory . $ticker . basename($fileName);
					
					if (! in_array($fileExtension,$fileExtensions)) {
						$errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
					}

					if ($fileSize > 2000000) {
						$errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
					}

					if (empty($errors)) {
						$didUpload = move_uploaded_file($fileTmpName, $uploadPath);

						if ($didUpload) {
							//echo "The file " . basename($fileName) . " has been uploaded";
						} else {
							echo "<script type='text/javascript'>alert('An error occurred somewhere. Try again or contact the admin');</script>";
						}
					} else {
						foreach ($errors as $error) {
							echo "<script type='text/javascript'>alert('. $error .');</script>";
							echo $error . "These are the errors" . "\n";
						}
					}
				}
			}

			else if($selected_val == '3'){
				$guid = $_SESSION['loggedIn'];
				$projname = $_POST['projname'];
				$ticker = $_POST['ticker'];
				$website = $_POST['website'];
				$logo = $ticker . $_FILES['myfile']['name'];
				$githublink = $_POST['githublink'];
				$btctalklink = $_POST['btctalklink'];
				$facebooklink = $_POST['facebooklink'];
				$twitterlink = $_POST['twitterlink'];
				$discordlink = $_POST['discordlink'];
				$telegramlink = $_POST['telegramlink'];
				$description = $_POST['description'];
				$specsalgo = $_POST['specsalgo'];
				$specspow = $_POST['specspow'];
				$totalcoinsissued = $_POST['totalcoinsissued'];
				$blocktime = $_POST['blocktime'];
				$blockreward = $_POST['blockreward'];
				$blockmature = $_POST['blockmature'];
				$masternodeonlinelink = $_POST['masternodeonlinelink'];
				$masternodecolatt = $_POST['masternodecolatt'];
				$maternodereward = $_POST['maternodereward'];
				$explorerlink = $_POST['explorerlink'];
				$exchanges = $_POST['exchange'];
				$kyd = $_POST['kydlink'];
				$whitepaper = $_POST['whitepaperlink'];
				$roadmap = $_POST['roadmaplink'];
				$launch = $_POST['launchdate'];
				$presale = $_POST['presalelink'];
				$roi = $_POST['roilink'];
				$txid = $_POST['txid'];
				$date = date("Y-m-d", strtotime(" +1 months"));
				
				$result0 = $mysqli->query("SELECT * FROM coins where symbol = '".$ticker."'");
				if($result0->num_rows == 0){
					$query = "INSERT INTO `coins`(user_guid, name, symbol, url, logo, approved, github, btctalk, facebook, twitter, discord, telegram, description, specsalgo, specspow, totalcoinsissued, blocktime, blockreward, blockmature, mno, masternodecolatt, maternodereward, explorer, exchanges, kyd, whitepaper, roadmap, launch, presale, roi, txid) VALUES ('$guid','$projname','$ticker','$website','$logo','0', '$githublink', '$btctalklink', '$facebooklink', '$twitterlink', '$discordlink', '$telegramlink', '$description', '$specsalgo', '$specspow', '$totalcoinsissued', '$blocktime', '$blockreward', '$blockmature', '$masternodeonlinelink', '$masternodecolatt', '$maternodereward', '$explorerlink', '$exchanges', '$kyd', '$whitepaper', '$roadmap', '$launch', '$presale', '$roi', '$txid')";
					$res = $mysqli->query($query);
					$query1 = "INSERT INTO `package`(approvedp, name, package, expire, txid, user_guid) VALUES ('0','$ticker','3','$date','$txid','$guid')";
					$res1 = $mysqli->query($query1);
					if ($FeatSel == 0){
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','2017-01-01','2017-01-01','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}elseif ($FeatSel == 1){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','1','$txid','0')";
						$res2 = $mysqli->query($query2);
					}else if ($FeatSel == 2){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','2','$txid','0')";
						$res2 = $mysqli->query($query2);					
					}else if ($FeatSel == 3){
						$date0 = date("Y-m-d");
						$date1 = date("Y-m-d", strtotime(" +7 days"));
						$query2 = "INSERT INTO `featured`(user_guid, name, start, end, tierpackage, txid, approvedf) VALUES ('$guid','$ticker','$date0','$date1','3','$txid','0')";
						$res2 = $mysqli->query($query2);
					}	
				
					$currentDir = getcwd();
					$uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/images/uploadsaltwiki324fs8ses3wefsF/';

					$errors = []; // Store all foreseen and unforseen errors here

					$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
					$fileExtension = '';
					$fileName = $_FILES['myfile']['name'];
					$fileSize = $_FILES['myfile']['size'];
					$fileTmpName  = $_FILES['myfile']['tmp_name'];
					$fileType = $_FILES['myfile']['type'];
					$tmp = explode('.', $fileName);
					$fileExtension= end($tmp);
					$fileExtension = strtolower($fileExtension);
					$uploadPath = $uploadDirectory . $ticker . basename($fileName);
					
					if (! in_array($fileExtension,$fileExtensions)) {
						$errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
					}

					if ($fileSize > 2000000) {
						$errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
					}

					if (empty($errors)) {
						$didUpload = move_uploaded_file($fileTmpName, $uploadPath);

						if ($didUpload) {
							//echo "The file " . basename($fileName) . " has been uploaded";
						} else {
							echo "<script type='text/javascript'>alert('An error occurred somewhere. Try again or contact the admin');</script>";
						}
					} else {
						foreach ($errors as $error) {
							echo "<script type='text/javascript'>alert('. $error .');</script>";
							echo $error . "These are the errors" . "\n";
						}
					}
				}
			}

		}
	}

    if(isset($_POST['modify']) && $_POST['modify'] === '1') {
        $selected_val =  $_POST['TierSel'];
        $id = $_POST['id'];

        if ($selected_val == '0') {
            $projname = $_POST['projname'];
            $ticker = $_POST['ticker'];
            $website = $_POST['website'];
            $specsalgo = $_POST['specsalgo'];
            $specspow = $_POST['specspow'];

            if (!preg_match("/^[a-zA-Z ]*$/",$projname)) {
                $nameErr = "Only letters and white space allowed";
            }
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
                $websiteErr = "Invalid URL";
            }

            if(strlen($_FILES['myfile']['name']) > 0 ){
                $logo = $ticker . $_FILES['myfile']['name'];
                $query = "UPDATE `coins` SET symbol= '$ticker', name= '$projname' , url='$website', specsalgo= '$specsalgo', specspow= '$specspow', logo='$logo' where id=".$_POST['id'];
                $res = mysqli_query($mysqli,$query);
                $currentDir = getcwd();
                $uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/images/uploadsaltwiki324fs8ses3wefsF/';

                $errors = []; // Store all foreseen and unforseen errors here

                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                $fileExtension = '';
                $fileName = $_FILES['myfile']['name'];
                $fileSize = $_FILES['myfile']['size'];
                $fileTmpName  = $_FILES['myfile']['tmp_name'];
                $fileType = $_FILES['myfile']['type'];
                $tmp = explode('.', $fileName);
                $fileExtension= end($tmp);
                $fileExtension = strtolower($fileExtension);
                $uploadPath = $uploadDirectory . $ticker . basename($fileName);

                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }

                if ($fileSize > 2000000) {
                    $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
                }

                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

                    if ($didUpload) {
                        //echo "The file " . basename($fileName) . " has been uploaded";
                    } else {
                        echo "<script type='text/javascript'>alert('An error occurred somewhere. Try again or contact the admin');</script>";
                    }
                } else {
                    foreach ($errors as $error) {
                        echo "<script type='text/javascript'>alert('. $error .');</script>";
                        echo $error . "These are the errors" . "\n";
                    }
                }
            }else{
                $query = "UPDATE `coins` SET symbol= '$ticker', name= '$projname' , url='$website', specsalgo= '$specsalgo', specspow= '$specspow' where id=".$_POST['id'];
                try {
                    $res = $mysqli->query($query);

                } catch (Exception $e) {
                    echo $e->getMessage();
                    exit;
                }

            }
        }

        elseif ($selected_val == '1') {
            $projname = $_POST['projname'];
            $ticker = $_POST['ticker'];
            $website = $_POST['website'];

            $githublink = $_POST['githublink'];
            $btctalklink = $_POST['btctalklink'];
            $facebooklink = $_POST['facebooklink'];
            $twitterlink = $_POST['twitterlink'];
            $discordlink = $_POST['discordlink'];
            $telegramlink = $_POST['telegramlink'];
            $specsalgo = $_POST['specsalgo'];
            $specspow = $_POST['specspow'];
            $date = date("Y-m-d", strtotime(" +1 months"));

            if (!preg_match("/^[a-zA-Z ]*$/",$projname)) {
                $nameErr = "Only letters and white space allowed";
            }
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
                $websiteErr = "Invalid URL";
            }

            if(strlen($_FILES['myfile']['name']) > 0 ){
                $logo = $ticker . $_FILES['myfile']['name'];
                $query = "UPDATE `coins` SET symbol= '$ticker', name= '$projname' , url='$website', specsalgo= '$specsalgo', specspow= '$specspow', logo='$logo',
                            github='$githublink', btctalk='$btctalklink', facebook='$facebooklink', twitter='$twitterlink', dicord='$discordlink', telegram='$telegramlink'
                          where id=".$_POST['id'];
                $res = mysqli_query($mysqli,$query);
                $currentDir = getcwd();
                $uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/images/uploadsaltwiki324fs8ses3wefsF/';

                $errors = []; // Store all foreseen and unforseen errors here

                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                $fileExtension = '';
                $fileName = $_FILES['myfile']['name'];
                $fileSize = $_FILES['myfile']['size'];
                $fileTmpName  = $_FILES['myfile']['tmp_name'];
                $fileType = $_FILES['myfile']['type'];
                $tmp = explode('.', $fileName);
                $fileExtension= end($tmp);
                $fileExtension = strtolower($fileExtension);
                $uploadPath = $uploadDirectory . $ticker . basename($fileName);

                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }

                if ($fileSize > 2000000) {
                    $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
                }

                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

                    if ($didUpload) {
                        //echo "The file " . basename($fileName) . " has been uploaded";
                    } else {
                        echo "<script type='text/javascript'>alert('An error occurred somewhere. Try again or contact the admin');</script>";
                    }
                } else {
                    foreach ($errors as $error) {
                        echo "<script type='text/javascript'>alert('. $error .');</script>";
                        echo $error . "These are the errors" . "\n";
                    }
                }
            }else{

                $query = "UPDATE `coins` SET symbol= '$ticker', name= '$projname' , url='$website', specsalgo= '$specsalgo', specspow= '$specspow',
                            github='$githublink', btctalk='$btctalklink', facebook='$facebooklink', twitter='$twitterlink', discord='$discordlink', telegram='$telegramlink'
                          where id=".$_POST['id'];
                try {
                    $res = $mysqli->query($query);

                } catch (Exception $e) {
                    echo $e->getMessage();
                    exit;
                }

            }
        }

        elseif ($selected_val == '2') {
            $projname = $_POST['projname'];
            $ticker = $_POST['ticker'];
            $website = $_POST['website'];
            $logo = $ticker . $_FILES['myfile']['name'];
            $githublink = $_POST['githublink'];
            $btctalklink = $_POST['btctalklink'];
            $facebooklink = $_POST['facebooklink'];
            $twitterlink = $_POST['twitterlink'];
            $discordlink = $_POST['discordlink'];
            $telegramlink = $_POST['telegramlink'];
            $description = $_POST['description'];
            $specsalgo = $_POST['specsalgo'];
            $specspow = $_POST['specspow'];
            $totalcoinsissued = $_POST['totalcoinsissued'];
            $blocktime = $_POST['blocktime'];
            $blockreward = $_POST['blockreward'];
            $blockmature = $_POST['blockmature'];
            $masternodeonlinelink = $_POST['masternodeonlinelink'];
            $masternodecolatt = $_POST['masternodecolatt'];
            $maternodereward = $_POST['maternodereward'];
            $explorerlink = $_POST['explorerlink'];
            $date = date("Y-m-d", strtotime(" +1 months"));

            if (!preg_match("/^[a-zA-Z ]*$/",$projname)) {
                $nameErr = "Only letters and white space allowed";
            }
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
                $websiteErr = "Invalid URL";
            }

            if(strlen($_FILES['myfile']['name']) > 0 ){
                $logo = $ticker . $_FILES['myfile']['name'];
                $query = "UPDATE `coins` SET symbol= '$ticker', name= '$projname' , url='$website', specsalgo= '$specsalgo', specspow= '$specspow', logo='$logo',
                            github='$githublink', btctalk='$btctalklink', facebook='$facebooklink', twitter='$twitterlink', dicord='$discordlink', telegram='$telegramlink',
                            description='$description', totalcoinsissued='$totalcoinsissued', blocktime='$blocktime', blockreward='$blockreward', blockmature='$blockmature',
                            masternodecolatt='$masternodecolatt', maternodereward='$masternodeonlinelink', explorer='$explorerlink',
                          where id=".$_POST['id'];
                $res = mysqli_query($mysqli,$query);
                $currentDir = getcwd();
                $uploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/images/uploadsaltwiki324fs8ses3wefsF/';

                $errors = []; // Store all foreseen and unforseen errors here

                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                $fileExtension = '';
                $fileName = $_FILES['myfile']['name'];
                $fileSize = $_FILES['myfile']['size'];
                $fileTmpName  = $_FILES['myfile']['tmp_name'];
                $fileType = $_FILES['myfile']['type'];
                $tmp = explode('.', $fileName);
                $fileExtension= end($tmp);
                $fileExtension = strtolower($fileExtension);
                $uploadPath = $uploadDirectory . $ticker . basename($fileName);

                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }

                if ($fileSize > 2000000) {
                    $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
                }

                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

                    if ($didUpload) {
                        //echo "The file " . basename($fileName) . " has been uploaded";
                    } else {
                        echo "<script type='text/javascript'>alert('An error occurred somewhere. Try again or contact the admin');</script>";
                    }
                } else {
                    foreach ($errors as $error) {
                        echo "<script type='text/javascript'>alert('. $error .');</script>";
                        echo $error . "These are the errors" . "\n";
                    }
                }
            }else{
var_dump($_POST);exit;
                $query = "UPDATE `coins` SET symbol= '$ticker', name= '$projname' , url='$website', specsalgo= '$specsalgo', specspow= '$specspow',
                            github='$githublink', btctalk='$btctalklink', facebook='$facebooklink', twitter='$twitterlink', discord='$discordlink', telegram='$telegramlink',
                            description='$description', totalcoinsissued='$totalcoinsissued', blocktime='$blocktime', blockreward='$blockreward', blockmature='$blockmature',
                            masternodecolatt='$masternodecolatt', maternodereward='$masternodeonlinelink', explorer='$explorerlink',
                          where id=".$_POST['id'];
                try {
                    $res = $mysqli->query($query);

                } catch (Exception $e) {
                    echo $e->getMessage();
                    exit;
                }

            }
        }

        elseif ($selected_val == '3') {

        }
    }

}
else if(isset($admin) && isset($_POST) && isset($_POST['whichapprove'])) {
	$actionon = $_POST['whichapprove'];
	$symbol = $_POST['symbol'];
	if($actionon == '0'){
		$query = "UPDATE `coins` SET approved='1' WHERE symbol='".$symbol."'";
		$res = $mysqli->query($query);
	}else if($actionon == '1'){
		$query = "UPDATE `package` SET approvedp='1' WHERE name='".$symbol."'";
		$res = $mysqli->query($query);		
	}else if($actionon == '2'){
		$query = "UPDATE `featured` SET approvedf='1' WHERE name='".$symbol."'";
		$res = $mysqli->query($query);		
	}
	header('Location: ./dashboard.php');
	exit();
}
else if(isset($admin) && isset($_POST) && isset($_POST['deletecoin'])) {
	$ticker = $_POST['symbol'];
	$query = "DELETE FROM coins WHERE symbol = '$ticker'";
	$res = $mysqli->query($query);
	$query1 = "DELETE FROM package WHERE name = '$ticker'";
	$res1 = $mysqli->query($query1);
	$query2 = "DELETE FROM featured WHERE name = '$ticker'";
	$res2 = $mysqli->query($query2);
	header('Location: ./dashboard.php');
	exit();
}
else if(isset($admin) && isset($_POST) && isset($_POST['onvoteing'])) {
	$ticker = $_POST['symbol'];
	$allowtxid = $_POST['allowedticker'];
	$query = "UPDATE `coins` SET allowreward='1', allowtxid='".$allowtxid."' WHERE symbol='".$ticker."'";
	$res = $mysqli->query($query);
}

    //foreach ($_POST as $key => $value) {

//        echo $key;
//		echo ":";
//        echo $value;
//		echo "<br>";
//    }
header('Location: ./dashboard.php');
exit();
?>