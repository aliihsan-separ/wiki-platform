<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

@session_start();

//require_once('database.php');

if(isset($_SESSION) && isset($_SESSION['loggedIn'])) {
    $seed = $_SESSION['loggedIn'];
    $query = "SELECT * FROM users WHERE guid = '$seed'";
	$query1 = "SELECT * FROM wiki_admin WHERE user_guid = '$seed'";
    if($res = $mysqli->query($query)) {
        if($u = $res->fetch_object()) {
            $user = $u;
        }
    }
    if($res1 = $mysqli->query($query1)) {
        if($u1 = $res1->fetch_object()) {
            $admin = $u1;
        }
    }
    unset($seed);
}
if(isset($user)) {



?>

<!DOCTYPE html>
<html>
<head>


<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="google-site-verification" content="WTETGxMIxI7ABZjKgVq3D56_RLE9MJU_UOC0XKh7NdU" />
    <title>Wiki Coin Platform</title>

    <link rel="icon" href="wikiicon.ico" type="image/x-icon"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.0.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/r-2.2.1/datatables.min.css"/>
	

	<script src="js/apps1.js"></script> 
    <link rel="stylesheet" href="style.css">
</head>

<body class="grey lighten-4" >


<nav class="light-blue darken-4">
    <div class="container">
        <div class="nav-wrapper">
			<a href="index.php" class="brand-logo">
			<img src="images\Site_logo_small.png" alt="WIKI Hub" class="responsive-img">
            </a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li class="active"><a href="index.php">Home</a></li>
				    <li><a href="https://wiki.exchange" target="_blank">Wiki Exchange</a></li>
				    <li><a href="tiers.php">Tiers</a></li>
				    <li><a href="https://github.com/Altcoinwiki/source/releases/tag/v1.1.0" target="_blank">Wallets</a></li>
				    <li><a href="http://explorer.altcoinwiki.io:3001/" target="_blank">Explorer</a></li>
				    <li><a href="vote.php">Vote</a></li>				
				    <li><a href="https://graviex.net/markets/wikbtc">Exchange</a></li>
				    <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="faq.php">FAQ</a></li>
				    <li><a href="about.php">About</a></li>
            <li><a href="logout.php">Logout</a></li>
            </ul>
        </div>
    </div>

</nav>

<?php
} else {
?>

<!DOCTYPE html>
<html>
<head>


<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="google-site-verification" content="WTETGxMIxI7ABZjKgVq3D56_RLE9MJU_UOC0XKh7NdU" />

    <title>Wiki Coin Platform</title>

    <link rel="icon" href="wikiicon.ico" type="image/x-icon"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.0.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/fh-3.1.3/r-2.2.1/datatables.min.css"/>
	

	<script src="js/apps1.js"></script> 
    <link rel="stylesheet" href="style.css">

</head>

<body class="grey lighten-4">


<nav class="light-blue darken-4">
    <div class="container">
        <div class="nav-wrapper">
			<a href="index.php" class="brand-logo">
			<img src="images\Site_logo_small.png" alt="WIKI Hub" class="responsive-img">
            </a>

            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li class="active"><a href="index.php">Home</a></li>
				<li><a href="exchange" target="_blank">Wiki Exchange</a></li>
				<li><a href="tiers.php">Tiers</a></li>
				<li><a href="https://github.com/Altcoinwiki/source/releases/tag/v1.1.0" target="_blank">Wallets</a></li>
				<li><a href="http://explorer.altcoinwiki.io:3001/" target="_blank">Explorer</a></li>
				<li><a href="vote.php">Vote</a></li>				
				<li><a href="https://graviex.net/markets/wikbtc">Exchange</a></li>
                <li><a href="faq.php">FAQ</a></li>
				<li><a href="about.php">About</a></li>
                <li><a href="dashboard.php">My Account</a></li>
            </ul>
        </div>
    </div>

</nav>


<?php
}
?>