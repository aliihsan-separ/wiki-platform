<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

$page = 'adminmode';
require_once('database.php');
require_once('header.php');
if(isset($_POST) && isset($_POST['seed'])) {
    $seed = $mysqli->escape_string($_POST['seed']);
    $query = "SELECT * FROM users WHERE guid = '$seed'";
    if($res = $mysqli->query($query)) {
        if($u = $res->fetch_object()) {
            $_SESSION['loggedIn'] = $seed;
        }
    }
}

$nameErr = $websiteErr = "";
$name = $website = "";

if(isset($user) && isset($admin)) {
?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h1>Admin Page</h1>
            </div>
        </div>
		<div class="row">
            <div class="col s12">
			<form action="procsub.php" method="post" id="actioncoin" >
                <table>
                    <thead>
                        <tr>
							<th>Package</th>
                            <th>Coin Ticker</th>
                            <th>Coin Name</th>
                            <th>Coin URL</th>
							<th>Coin Logo</th>
							<th>Date Expire</th>
							<th>Coin Approved</th>
							<th>Package Approved</th>
							<th>Featured Coin Status</th>
							<th>Action</th>
                        </tr>
<?php
	if(isset($_POST) && isset($_POST['groupa'])) {
		$ticker = $_POST['groupa'];
		$result = $mysqli->query("SELECT * from package where name = '".$ticker."'");
		if($result){
			while($row = $result->fetch_object()) 
			{
				$package = $row->package;
				if($package == 0){
					$packagename = 'FREE';
					//**************************************
					//FREE PACKAGE
					//**************************************					
					$result0 = $mysqli->query("SELECT t1.symbol, t1.name, t1.logo, t1.url, t1.approved, t3.approvedf, t2.approvedp, t1.allowreward FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t1.symbol = '".$ticker."'");
					if($result0){
						while($row0 = $result0->fetch_object()) 
						{	
							$symbol0 = $row0->symbol;
							$name0 = $row0->name;
							$logo0 = $row0->logo;
							$url0 = $row0->url;
							$approved0 = $row0->approved;
							$approvedf0 = $row0->approvedf;
							$approvedp0 = $row0->approvedp;
							$reward0 = $row0->allowreward;
							if($reward0 == '0'){
								$rewardname0 = 'NOT SUBMITTED';
							} elseif($reward0 == '1') {
								$rewardname0 = 'APPROVED';
							} elseif($reward0 == '2') {
								$rewardname0 = 'WAITING';
							}
							if($approvedf0 == '0'){
								$approvedf0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedf0 == '1') {
								$approvedf0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}		
							if($approvedp0 == '0'){
								$approvedp0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedp0 == '1') {
								$approvedp0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}
							if($approved0 == '0'){
								$approved0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approved0 == '1') {
								$approved0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}							
?>

						<tr>
							<th><?php echo $packagename; ?></th>
	                        <th><?php echo $symbol0; ?></th>
                            <th><?php echo $name0; ?></th>
                            <th><?php echo '        <center><img src="images\coins\\' . $logo0 . '" alt="" height="100" width="100"></center><input type="hidden" name="symbol" id="hiddenField" value="'.$symbol0.'" />'; ?></th>
							<th><?php echo $url0; ?></th>
							<th><?php echo "2999-01-01"; ?></th>
							<th><?php echo $approved0stat; ?></th>
							<th><?php echo $approvedp0stat; ?></th>
							<th><?php echo $approvedf0stat; ?></th>		
							<th><?php echo $rewardname0; ?></th>
<?php
							if($approved0 == 0){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Coin</button><input type="hidden" name="whichapprove" id="hiddenField" value="0" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '0' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Package</button><input type="hidden" name="whichapprove" id="hiddenField" value="1" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '1' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Featured</button><input type="hidden" name="whichapprove" id="hiddenField" value="2" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else{
								echo '<th><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}
							echo "<th><button class='light-blue darken-4 btn' type='submit' name='onvoteing' >Approve Voting</button><input name=allowedticker id=allowedticker maxlength=255 type=text class=form-control placeholder='Reward TXID' pattern='^[a-zA-Z0-9\\s]+' /></th>";
?>							
						</tr>

<?php							
						}
					}else{
						printf("Problem with SQL Query: %s", $mysqli->error);						
					}
				}else if($package == '1') {
					$packagename = 'BRONZE';
					//**************************************
					//BRONZE PACKAGE
					//**************************************					
					$result0 = $mysqli->query("SELECT t1.symbol, t1.name, t1.logo, t1.url, t1.approved, t3.approvedf, t2.approvedp, t2.expire, t1.allowreward FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t1.symbol = '".$ticker."'");
					if($result0){
						while($row0 = $result0->fetch_object()) 
						{	
							$symbol0 = $row0->symbol;
							$name0 = $row0->name;
							$logo0 = $row0->logo;
							$url0 = $row0->url;
							$approved0 = $row0->approved;
							$approvedf0 = $row0->approvedf;
							$approvedp0 = $row0->approvedp;
							$expire0 = $row0->expire;
							$reward = $row0->allowreward;
							if($reward == '0'){
								$rewardname0 = 'NOT SUBMITTED';
							} elseif($reward == '1') {
								$rewardname0 = 'APPROVED';
							} elseif($reward == '2') {
								$rewardname0 = 'WAITING';
							}							
							if($approvedf0 == '0'){
								$approvedf0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedf0 == '1') {
								$approvedf0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}		
							if($approvedp0 == '0'){
								$approvedp0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedp0 == '1') {
								$approvedp0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}
							if($approved0 == '0'){
								$approved0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approved0 == '1') {
								$approved0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}

?>

						<tr>
							<th><?php echo $packagename; ?></th>
	                        <th><?php echo $symbol0; ?></th>
                            <th><?php echo $name0; ?></th>
                            <th><?php echo '        <center><img src="images\coins\\' . $logo0 . '" alt="" height="100" width="100"></center><input type="hidden" name="symbol" id="hiddenField" value="'.$symbol0.'" />'; ?></th>
							<th><?php echo $url0; ?></th>
							<th><?php echo $expire0; ?></th>
							<th><?php echo $approved0stat; ?></th>
							<th><?php echo $approvedp0stat; ?></th>
							<th><?php echo $approvedf0stat; ?></th>		
<?php
							if($approved0 == 0){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Coin</button><input type="hidden" name="whichapprove" id="hiddenField" value="0" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '0' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Package</button><input type="hidden" name="whichapprove" id="hiddenField" value="1" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '1' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Featured</button><input type="hidden" name="whichapprove" id="hiddenField" value="2" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else{
								echo '<th><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}
							echo "<th><button class='light-blue darken-4 btn' type='submit' name='onvoteing' >Approve Voting</button><input name=allowedticker id=allowedticker maxlength=255 type=text class=form-control placeholder='Reward TXID' pattern='^[a-zA-Z0-9\\s]+' /></th>";
?>							
						</tr>

<?php							
						}
					}else{
						printf("Problem with SQL Query: %s", $mysqli->error);						
					}					
				} elseif($package == '2') {
					//**************************************
					//SILVER PACKAGE
					//**************************************
					$packagename = 'SILVER';
					$result0 = $mysqli->query("SELECT t1.symbol, t1.name, t1.logo, t1.url, t1.approved, t3.approvedf, t2.approvedp, t1.allowreward FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t1.symbol = '".$ticker."'");
					if($result0){
						while($row0 = $result0->fetch_object()) 
						{	
							$symbol0 = $row0->symbol;
							$name0 = $row0->name;
							$logo0 = $row0->logo;
							$url0 = $row0->url;
							$approved0 = $row0->approved;
							$approvedf0 = $row0->approvedf;
							$approvedp0 = $row0->approvedp;
							$reward = $row0->allowreward;
							if($reward == '0'){
								$rewardname0 = 'NOT SUBMITTED';
							} elseif($reward == '1') {
								$rewardname0 = 'APPROVED';
							} elseif($reward == '2') {
								$rewardname0 = 'WAITING';
							}							
							if($approvedf0 == '0'){
								$approvedf0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedf0 == '1') {
								$approvedf0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}		
							if($approvedp0 == '0'){
								$approvedp0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedp0 == '1') {
								$approvedp0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}
							if($approved0 == '0'){
								$approved0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approved0 == '1') {
								$approved0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}

?>

						<tr>
							<th><?php echo $packagename; ?></th>
	                        <th><?php echo $symbol0; ?></th>
                            <th><?php echo $name0; ?></th>
                            <th><?php echo '        <center><img src="images\coins\\' . $logo0 . '" alt="" height="100" width="100"></center><input type="hidden" name="symbol" id="hiddenField" value="'.$symbol0.'" />'; ?></th>
							<th><?php echo $url0; ?></th>
							<th><?php echo $approved0stat; ?></th>
							<th><?php echo $approvedp0stat; ?></th>
							<th><?php echo $approvedf0stat; ?></th>		
<?php
							if($approved0 == 0){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Coin</button><input type="hidden" name="whichapprove" id="hiddenField" value="0" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '0' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Package</button><input type="hidden" name="whichapprove" id="hiddenField" value="1" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '1' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Featured</button><input type="hidden" name="whichapprove" id="hiddenField" value="2" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else{
								echo '<th><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}
							echo "<th><button class='light-blue darken-4 btn' type='submit' name='onvoteing' >Approve Voting</button><input name=allowedticker id=allowedticker maxlength=255 type=text class=form-control placeholder='Reward TXID' pattern='^[a-zA-Z0-9\\s]+' /></th>";
?>							
						</tr>

<?php							
						}
					}else{
						printf("Problem with SQL Query: %s", $mysqli->error);						
					}					
				} elseif($package == '3') {
					$packagename = 'GOLD';
					//**************************************
					//GOLD PACKAGE
					//**************************************
					$result0 = $mysqli->query("SELECT t1.symbol, t1.name, t1.logo, t1.url, t1.approved, t3.approvedf, t2.approvedp, t1.allowreward FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t1.symbol = '".$ticker."'");
					if($result0){
						while($row0 = $result0->fetch_object()) 
						{	
							$symbol0 = $row0->symbol;
							$name0 = $row0->name;
							$logo0 = $row0->logo;
							$url0 = $row0->url;
							$approved0 = $row0->approved;
							$approvedf0 = $row0->approvedf;
							$approvedp0 = $row0->approvedp;
							$reward = $row0->allowreward;
							if($reward == '0'){
								$rewardname0 = 'NOT SUBMITTED';
							} elseif($reward == '1') {
								$rewardname0 = 'APPROVED';
							} elseif($reward == '2') {
								$rewardname0 = 'WAITING';
							}							
							if($approvedf0 == '0'){
								$approvedf0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedf0 == '1') {
								$approvedf0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}		
							if($approvedp0 == '0'){
								$approvedp0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approvedp0 == '1') {
								$approvedp0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}
							if($approved0 == '0'){
								$approved0stat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
							} elseif($approved0 == '1') {
								$approved0stat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
							}							
?>

						<tr>
							<th><?php echo $packagename; ?></th>
	                        <th><?php echo $symbol0; ?></th>
                            <th><?php echo $name0; ?></th>
                            <th><?php echo '        <center><img src="images\coins\\' . $logo0 . '" alt="" height="100" width="100"></center><input type="hidden" name="symbol" id="hiddenField" value="'.$symbol0.'" />'; ?></th>
							<th><?php echo $url0; ?></th>
							<th><?php echo $approved0stat; ?></th>
							<th><?php echo $approvedp0stat; ?></th>
							<th><?php echo $approvedf0stat; ?></th>		
<?php
							if($approved0 == 0){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Coin</button><input type="hidden" name="whichapprove" id="hiddenField" value="0" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '0' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Package</button><input type="hidden" name="whichapprove" id="hiddenField" value="1" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else if($approvedp0 == '1' && $approved0 == '1' && $approvedf0 == '0'){
								echo '<th><button type="submit" class="light-blue darken-4 btn btn-primary center-block">Approve Featured</button><input type="hidden" name="whichapprove" id="hiddenField" value="2" /><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}else{
								echo '<th><button class="light-blue darken-4 btn" type="submit" name="deletecoin" onclick="return confirm("Delete this record from database?");">Delete Selected Coin</button></th>';
							}
							echo "<th><button class='light-blue darken-4 btn' type='submit' name='onvoteing' >Approve Voting</button><input name=allowedticker id=allowedticker maxlength=255 type=text class=form-control placeholder='Reward TXID' pattern='^[a-zA-Z0-9\\s]+' /></th>";
?>							
						</tr>

<?php							
						}
					}else{
						printf("Problem with SQL Query: %s", $mysqli->error);						
					}					
				}
			}
		}
	}
        
?>		

		                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</form>
            </div>
        </div>
    </div>
</div>			
		
		

<?php
}
require_once('footer.php');
?>		