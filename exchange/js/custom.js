(function($) {
  'use strict';

  var $elem, ticker, btcData;

  /* document ready all function here */
  $(document).ready(function() {
    $('.counter').counterUp({
      delay: 10,
      time: 2000
    });

    $('.dropdown').on('show.bs.dropdown', function(e) {
      $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
    });

    $('.dropdown').on('hide.bs.dropdown', function(e) {
      $(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
    });

    $(".dropdown-menu-coin1 a").on('click', function() {
      $("#dropdowncoin1 .selected-coin1").text($(this).text());
    });

    $(".dropdown-menu-coin2 a").on('click', function() {
      $("#dropdowncoin2 .selected-coin2").text($(this).text());
    });

    $(".dropdown-menu-coin3 a").on('click', function() {
      $("#dropdowncoin3 .selected-coin3").text($(this).text());
    });

    $(".dropdwon-language a").on('click', function() {
      $("#dropdownlanguage .selected-language").html($(this).html());
    });

    $('#go-to-top').each(function() {
      $(this).on('click', function() {
        $('html,body').animate({
          scrollTop: 0
        }, 'slow');
        return false;
      });
    });

    $.getJSON('https://poloniex.com/public?command=returnTicker', function (res) {
      $('.js-ticker').each(function (elem) {
        $elem = $(this);
        ticker = $elem.data('coin');
        btcData = res["USDT_" + ticker];

        $elem.children('.js-price').html('$'+(+btcData.last).toFixed(2) +' USD <span style="color: ' + (+btcData.percentChange < 0? 'red' : 'green') + '">('+ (+btcData.percentChange).toFixed(2) +'%)</span>');
      });
    });

  });

})(jQuery)