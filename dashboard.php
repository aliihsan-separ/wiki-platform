<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

$page = 'dashboard';
require_once('database.php');
require_once('header.php');
if (isset($_POST) && isset($_POST['seed'])) {
    $seed = $mysqli->escape_string($_POST['seed']);
    $query = "SELECT * FROM users WHERE guid = '$seed'";
    if ($res = $mysqli->query($query)) {
        if ($u = $res->fetch_object()) {
            $_SESSION['loggedIn'] = $seed;
        }
    }
}

$nameErr = $websiteErr = "";
$name = $website = "";
if (isset($_POST) && isset($user) && isset($_POST['submit'])) {
    $ticker = $_POST['group'];
    $query = "DELETE FROM coins WHERE symbol = '$ticker'";
    $res = $mysqli->query($query);
    $query1 = "DELETE FROM package WHERE name = '$ticker'";
    $res1 = $mysqli->query($query1);
    $query2 = "DELETE FROM featured WHERE name = '$ticker'";
    $res2 = $mysqli->query($query2);
}

if (isset($_POST) && isset($user) && isset($_POST['votecoin'])) {
    $ticker = $_POST['group'];
    $query = "UPDATE `coins` SET allowreward='2' WHERE symbol='" . $ticker . "'";
    $res = $mysqli->query($query);
}

if (isset($user) && !isset($admin)) {
    ?>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h1>My Coins</h1>
                </div>
            </div>
            <form name="trigdelete" action="dashboard.php" method="post">
                <div class="row">
                    <div class="col s12">

                        <table>
                            <thead>
                            <tr>
                                <th>Coin Name</th>
                                <th>Tier Assigned</th>
                                <th>Tier Expire Status</th>
                                <th>Coin Approved</th>
                                <th>Package Approved</th>
                                <th>Featured Coin Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            $packages=[];
                            $result = $mysqli->query("SELECT t1.id, t1.symbol, t1.name, t2.package, t2.expire, t3.approvedf, t1.approved, t2.id, t2.approvedp,t3.tierpackage FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t1.user_guid = '" . $_SESSION['loggedIn'] . "'");
                            if ($result) {
                                while ($row = $result->fetch_object()) {
                                    $ticker = $row->symbol;
                                    $name = $row->name;
                                    $package = $row->package;
                                    $packages[] = $row->tierpackage;
                                    $expire = $row->expire;
                                    $featured = $row->approvedf;
                                    $approvedp = $row->approvedp;
                                    $approved = $row->approved;
                                    if ($package == '0') {
                                        $packagename = 'FREE';
                                    } elseif ($package == '1') {
                                        $packagename = 'BRONZE';
                                    } elseif ($package == '2') {
                                        $packagename = 'SILVER';
                                    } elseif ($package == '3') {
                                        $packagename = 'GOLD';
                                    }
                                    if ($featured == '0') {
                                        $featuredstat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
                                    } elseif ($featured == '1') {
                                        $featuredstat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
                                    }
                                    if ($approvedp == '0') {
                                        $approvedpstat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
                                    } elseif ($approvedp == '1') {
                                        $approvedpstat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
                                    }
                                    if ($approved == '0') {
                                        $approvedstat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
                                    } elseif ($approved == '1') {
                                        $approvedstat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
                                    }
                                    echo '
                            <tr>
                                <td>
                                    <label>
                                        <input class="with-gap" name="group" id="' . $i . '" value="' . $ticker . '"  data-id="' . $row->id . '" type="radio"  />
                                        <span>' . $name . '</span>
                                    </label>
                                </td>
                                <td>' . $packagename . '</td>
                                <td>' . $expire . '</td>
                                <td>' . $approvedstat . '</td>
                                <td>' . $approvedpstat . '</td>
                                <td>' . $featuredstat . '</td>
                            </tr>';
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <button class="light-blue darken-4 btn" type="submit" name="submit"
                                onclick="return confirm('Delete this record from database?');">Delete Selected Coin
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <button class="light-blue darken-4 btn" type="submit" name="votecoin">Add Voting</button>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-sm-2">
                    <button data-toggle="modal" data-target="#newcoinmodal"
                            class="light-blue darken-4 btn btn-primary center-block">Add New Coin
                    </button>
                </div>
                <div class="col-sm-2">
                    <button data-toggle="modal" data-target="#modifySelectedModal" id="modifyButton"
                            class="light-blue darken-4 btn btn-primary center-block">Modify Selected Coin
                    </button>
                </div>

                    <div class="col-sm-2">
                        <button data-toggle="modal" data-target="#listOnExchangeModal"
                                class="light-blue darken-4 btn btn-primary center-block">List On Exchange
                        </button>
                    </div>

            </div>

        </div>
    </div>

    <div class="modal fade addModal" id="newcoinmodal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialogauto">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">Add a New Coin</h3>
                </div>
                <div class="modal-body">

                    <!-- Steps starts here -->
                    <div class="requestwizard">
                        <div class="requestwizard-row setup-panel">
                            <div class="requestwizard-step">
                                <a href="#step-1" type="button"
                                   class="light-blue darken-4 btn btn-primary btn-circle">1</a>
                                <p>Select Tier</p>
                            </div>
                            <div class="requestwizard-step">
                                <a href="#step-2" type="button" class="light-blue darken-4 btn btn-default btn-circle"
                                   disabled="disabled">2</a>
                                <p>Project information</p>
                            </div>
                            <div class="requestwizard-step">
                                <a href="#step-3" type="button" class="light-blue darken-4 btn btn-default btn-circle"
                                   disabled="disabled">3</a>
                                <p>Optional Features</p>
                            </div>
                            <div class="requestwizard-step">
                                <a href="#step-4" type="button" class="light-blue darken-4 btn btn-default btn-circle"
                                   disabled="disabled">4</a>
                                <p>Terms and Conditions</p>
                            </div>
                            <div class="requestwizard-step">
                                <a href="#step-5" type="button" class="light-blue darken-4 btn btn-default btn-circle"
                                   disabled="disabled">5</a>
                                <p>Payment</p>
                            </div>
                        </div>
                    </div>

                    <form role="form" id="tierspackage" action="procsub.php" method="post"
                          enctype="multipart/form-data">
                        <div class="row setup-content" id="step-1">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control form-control-lg" name="TierSel" id="TierSel"
                                                onchange="ChangecatList()">
                                            <option value="" disabled="disabled" selected="selected">Choose Tier ...
                                            </option>
                                            <option value="0">Free</option>
                                            <option value="1">Bronze</option>
                                            <option value="2">Silver</option>
                                            <option value="3">Gold</option>
                                        </select>
                                    </div>
                                    <button class="light-blue darken-4 btn btn-primary nextBtn btn-lg pull-right"
                                            type="button">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row setup-content" id="step-2">
                            <div class="col-lg-12">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <p id="projectinfo"></p>
                                    </div>
                                    <button class="light-blue darken-4 btn btn-primary nextBtn btn-lg pull-right"
                                            type="button">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row setup-content" id="step-3">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Optional Coin Features</label>
                                        <div>Choose if you want to add your coin to the featured section of the site.
                                        </div>
                                        <br>
                                        <select class="form-control form-control-lg" name="feat" id="feat"
                                                onchange="ChangecatList1()">
                                            <option value="" disabled="disabled" selected="selected">Choose Feature
                                                ...
                                            </option>
                                            <option value="0">None</option>
                                            <option value="1">Featured top 5 for 7 days 100 Wikicoins</option>
                                            <option value="2">Featured top 5 - 10 for 7 days 73 Wikicoins</option>
                                            <option value="3">Featured top 10 - 20 for 7 days 37 Wikicoins</option>
                                            <option value="4">List on Wiki.Exchange 1 month 5000 Wiki</option>
                                        </select>
                                    </div>
                                    <button class="light-blue darken-4 btn btn-primary nextBtn btn-lg pull-right"
                                            type="button">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row setup-content" id="step-4">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Terms and Conditions</label>
                                        <div>By using this platform you agree to the terms and conditions of Wiki Coin
                                            Platform.
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="checkbox-custom checkbox-inline"
                                                       data-initialize="checkbox" id="myCheckbox5"><input
                                                            type="checkbox" class="sr-only" required name="terms"
                                                            id="terms"><span class="checkbox-label"> I accept the <u><a
                                                                    href="tos.php"
                                                                    target="_blank">Terms and Conditions</a></u></span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="light-blue darken-4 btn btn-primary nextBtn btn-lg pull-right"
                                            type="button">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row setup-content" id="step-5">
                            <div class="col-lg-12">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <p id="paymentinfo"></p>
                                    </div>
                                    <button class="light-blue darken-4 btn" type="submit">Submit form</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade selectModal" id="modifySelectedModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title" id="lineModalLabel">Modify <?= $name ?></h3>
                </div>
                <div class="modal-body">

                    <form role="form" id="modifiyForm" action="procsub.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <p id="modify-projectinfo"></p>
                                </div>
                                <input type="hidden" name="modify" value="1">
                                <button class="light-blue darken-4 btn btn-primary nextBtn btn-lg pull-right" type="submit"> Update  </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="listOnExchangeModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialogauto">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">List On Exhange</h3>
                </div>
                <div class="modal-body">

                    <form role="form" id="listOnExchange" action="PHPMailer/send.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="coin_name"> Coin Name : </label>
                                        <input type="text" id="coin_name" name="coin_name" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="ticker"> Ticker : </label>
                                        <input type="text" id="ticker" name="ticker" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="algo"> Algo : </label>
                                        <select class=browser-default id=algo name=algo required autocomplete="off">
                                            <option value='' disabled selected>Choose your option</option>
                                            <?php
                                            $algos = ["aergo", "allium", "bcd", "bitcore", "blake2s", "blakecoin", "c11", "groestl", "hex", "hmq1725", "keccak", "keccakc", "lbk3", "lbry", "lyra2v2", "lyra2z", "m7m", "myr-gr", "neoscrypt", "nist5", "phi", "phi2", "quark", "qubit", "scrypt", "sib", "skein", "skunk", "tribus", "x11", "x16r", "x16s", "x17", "x22i", "xevan", "yescrypt", "sha256"];
                                               $i=0;
                                                foreach ($algos as $algo){
                                                    echo '<option value="'.$i.'">'.$algo.'</option>';
                                                    $i++;
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="codebase"> Codebase ( for example : Pivx, Dash etc ) : </label>
                                        <input type="text" id="codebase" name="codebase" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="explorer"> Block Explorer : </label>
                                        <input type="text" id="explorer" name="explorer" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="website"> Official Website : </label>
                                        <input type="text" id="website" name="website" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="btctalk"> Bitcointalk ann : </label>
                                        <input type="text" id="btctalk" name="btctalk" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="github"> Github source : </label>
                                        <input type="text" id="github" name="github" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="discord"> Discord server : </label>
                                        <input type="text" id="discord" name="discord" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="twitter"> Twitter : </label>
                                        <input type="text" id="twitter" name="twitter" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <label>Link to logo</label>
                                        <div class='file-field input-field'>
                                            <div class='light-blue darken-4 btn'>
                                                <span>Browse</span><input type='file' name='myfile' id='fileToUpload' required/>
                                            </div>
                                            <div class='file-path-wrapper'>
                                                <input class='file-path validate' type='text' placeholder='Upload file'/>
                                                <small>minimum 200x200px, transparent PNG required</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="supply"> Maximum supply : </label>
                                        <input type="text" id="supply" name="supply" required autocomplete="off">
                                    </div>

                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="email"> Your email address : </label>
                                        <input type="text" id="email" name="email" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <label for="discord_name"> Your contact username on discord including the # number : </label>
                                        <input type="text" id="discord_name" name="discord_name" required autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <?php
                                            $wallet = ['WesaHVy7aXkvhgjHVReVLsug8qFhcqT1f4', 'WRCjhMZjesR61bfKDCpKPba739mVsLqwQc', 'Wi6eNQx1kuPoLR2S9aL5maFypsfzewYQGo'];
                                            $random = random_int(0,2);
                                        ?>
                                        <label class='control-label'>Please pay 5000 WikiCoins to <?=  $wallet[$random] ?> </label>
                                        <input name='txid' id='txid' maxlength='100' type='text' required autocomplete="off" class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />
                                    </div>
                                    <div class="form-group col-md-9 col-xs-9">
                                        <div class="form-check">
                                            <label class="checkbox-custom checkbox-inline"
                                                   data-initialize="checkbox" id="myCheckbox5">
                                                <input type="checkbox" class="sr-only" required name="terms" id="terms">
                                                <span class="checkbox-label">
                                                    I accept the
                                                    <u><a href="tos.php" target="_blank">Terms and Conditions</a></u>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="mail" value="1">
                                    <div class="form-group col-md-3 col-xs-3">
                                        <div class="form-check">
                                            <button class="light-blue darken-4 btn btn-primary nextBtn btn-lg pull-right"
                                                    type="submit">Submit
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


<!-- support@wiki.exchange / :xq1NCO7rhk*[  -->
    <?php
} elseif (isset($user) && isset($admin)) {
    ?>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h1>Wiki Coin Admin</h1>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <form action="adminmode.php" method="post" id="adminm">
                        <table>
                            <thead>
                            <tr>
                                <th>Coin Name</th>
                                <th>Tier Assigned</th>
                                <th>Coin Approved</th>
                                <th>Package Approved</th>
                                <th>Featured Coin Approved</th>
                                <th>Voting Approved</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $result = $mysqli->query("SELECT t1.symbol, t1.name, t2.package, t2.expire, t3.approvedf, t1.approved, t2.approvedp, t1.allowreward FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name");
                            if ($result) {
                                while ($row = $result->fetch_object()) {
                                    $ticker = $row->symbol;
                                    $name = $row->name;
                                    $package = $row->package;
                                    $expire = $row->expire;
                                    $featured = $row->approvedf;
                                    $approvedp = $row->approvedp;
                                    $approved = $row->approved;
                                    $reward = $row->allowreward;
                                    if ($reward == '0') {
                                        $rewardname = 'NOT SUBMITTED';
                                    } elseif ($reward == '1') {
                                        $rewardname = 'APPROVED';
                                    } elseif ($reward == '2') {
                                        $rewardname = 'WAITING';
                                    }
                                    if ($package == '0') {
                                        $packagename = 'FREE';
                                    } elseif ($package == '1') {
                                        $packagename = 'BRONZE';
                                    } elseif ($package == '2') {
                                        $packagename = 'SILVER';
                                    } elseif ($package == '3') {
                                        $packagename = 'GOLD';
                                    }
                                    if ($featured == '0') {
                                        $featuredstat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
                                    } elseif ($featured == '1') {
                                        $featuredstat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
                                    }
                                    if ($approvedp == '0') {
                                        $approvedpstat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
                                    } elseif ($approvedp == '1') {
                                        $approvedpstat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
                                    }
                                    if ($approved == '0') {
                                        $approvedstat = '<span class="glyphicon glyphicon-remove-sign" style="color:red"></span>';
                                    } elseif ($approved == '1') {
                                        $approvedstat = '<span class="glyphicon glyphicon-ok-sign" style="color:green"></span>';
                                    }
                                    echo '<tr><td><label><input class="with-gap" name="groupa" value="' . $ticker . '" type="radio" checked /><span>' . $name . '</span></label></td><td>' . $packagename . '</td><td>' . $approvedstat . '</td><td>' . $approvedpstat . '</td><td>' . $featuredstat . '</td><td>' . $rewardname . '</td></tr>';
                                }
                            } else {
                                echo '<tr><td colspan="3">You don\'t have any active coins currently. Click the Add New Coin button to start.</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="row"></div>
                        <div class="row">
                            <div class="col s12">
                                <button class="light-blue darken-4 btn btn-primary center-block">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php
} else {
    ?>

    <div class="main" style="margin-top: 40px;">
        <div class="container">
            <div class="row">
                <div class="col s12 m6">
                    <div class="card-panel">
                        <h4>Login</h4>
                        <p>Sign in with your existing password.</p>

                        <form method="post" id="login">
                            <label for="seed">Password</label>
                            <input id="seed" name="seed" type="text" class="browser-default">

                            <p class="center-align"><input type="submit" href="#" class="light-blue darken-4 btn"
                                                           value="Enter Account"/></p>
                        </form>
                    </div>
                </div>
                <div class="col s12 m6">
                    <div class="card-panel">
                        <h4>Register</h4>
                        <p>Create a new password with your email.</p>

                        <form id="register">
                            <label for="email">Email</label>
                            <input id="email" type="email" class="browser-default">
                            <p class="center-align"><input type="submit" href="#" class="light-blue darken-4 btn"
                                                           value="Create Account"/></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="regModal" class="modal">
        <div class="modal-content">
            <h4>Account created</h4>
            <p>Your account password is:</p>
            <pre id="seedRes" style="font-weight: bold;"></pre>
            <p>Please save this as there is no way to recover the password after this point.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
        </div>
    </div>

    <?php
}
require_once('footer.php');
?>