<?php
session_start();

require_once('database.php');

if(isset($_POST) && isset($_POST['seed'])) {
    $seed = $mysqli->escape_string($_POST['seed']);
    $query = "SELECT * FROM users WHERE seed = '$seed'";

    if($res = $mysqli->query($query)) {
        if($user = $res->fetch_object()) {
            $_SESSION['loggedIn'] = $user;
        }
    }
}

header('Location: ./dashboard.php');