<?php
$page = 'home';

require_once('database.php');
require_once('header.php');
$decoded_json = json_decode(file_get_contents("https://api.coinmarketcap.com/v1/ticker/"), TRUE);

function price($curr) {

	$someArray = $decoded_json = json_decode(file_get_contents("https://api.coinmarketcap.com/v2/listings/"), TRUE);
	//print_r($someArray);
	
		foreach ($someArray["data"] as $key => $value) {
			$coin = $value["symbol"];
			if($coin == $curr){
				$id = $value["id"];
			}
		}
		if(isset($id)){
			$PriceArray = $decoded_json = json_decode(file_get_contents("https://api.coinmarketcap.com/v2/ticker/" . $id . "/"), TRUE);
			if(isset($PriceArray["data"]["quotes"]["USD"]["price"])){
				return $PriceArray["data"]["quotes"]["USD"]["price"];
			}
		}else{
			return "N/A";
		}

}
function avg($curr) {

	$someArray = $decoded_json = json_decode(file_get_contents("https://api.coinmarketcap.com/v2/listings/"), TRUE);
	//print_r($someArray);
	
		foreach ($someArray["data"] as $key => $value) {
			$coin = $value["symbol"];
			if($coin == $curr){
				$id = $value["id"];
			}
		}
		if(isset($id)){
			$PriceArray = $decoded_json = json_decode(file_get_contents("https://api.coinmarketcap.com/v2/ticker/" . $id . "/"), TRUE);

			if(isset($PriceArray["data"]["quotes"]["USD"]["percent_change_24h"])){
				return $PriceArray["data"]["quotes"]["USD"]["percent_change_24h"];
			}
		}else{
			return "N/A";
		}

}

if(isset($_POST) && isset($_POST['symbol'])) {
	
}else{
	header('Location: ./index.php');
}

						$result = $mysqli->query("SELECT t1.*, t2.package FROM coins t1 inner join package t2 on t1.symbol = t2.name where t1.symbol = '".$_POST['symbol']."' and t1.approved = 1");
						if($result) 
						{
							while($row = $result->fetch_object()) 
							{
								$name = $row->name;	
								$symbol = $row->symbol;                   
								$logo1 = $row->logo;   
								$logo = str_replace(' ', '%20', $logo1);
								$url = $row->url; 
								$github = $row->github;
								$btctalk = $row->btctalk;
								$facebook = $row->facebook;
								$twitter = $row->twitter;
								$discord = $row->discord;
								$telegram = $row->telegram;
								$description = $row->description;
								$specsalgo = $row->specsalgo;;
								$specspow = $row->specspow;
								$totalcoinsissued = $row->totalcoinsissued;
								$totalcoinsissuedout = number_format($totalcoinsissued);
								$blocktime = $row->blocktime;
								$blockreward = $row->blockreward;
								$blockmature = $row->blockmature;
								$masternodeonlinelink = $row->mno;
								$masternodecolatt = $row->masternodecolatt;
								$maternodereward = $row->maternodereward;
								$explorerlink = $row->explorer;
								$exchanges = $row->exchanges;
								$kyd = $row->kyd;
								$whitepaper = $row->whitepaper;
								$roadmap = $row->roadmap;
								$launch = $row->launch;
								$presale = $row->presale;
								$roi = $row->roi;
								$package = $row->package;

								if($specspow == '1'){
									$specspowout = "POW";
								}else if($specspow == '2'){
									$specspowout = "POS";
								}else if($specspow == '3'){
									$specspowout = "POW & POS";
								}else if($specspow == '4'){
									$specspowout = "POW & MN";
								}else if($specspow == '5'){									
									$specspowout = "POS & MN";	
								}								
								if($specsalgo == '1'){
									$specsalgoout = "Aergo";
								}else if($specsalgo == '2'){
									$specsalgoout = "Allium";
								}else if($specsalgo == '3'){
									$specsalgoout = "Bcd";
								}else if($specsalgo == '4'){
									$specsalgoout = "Bitcore";
								}else if($specsalgo == '5'){
									$specsalgoout = "Blake2s";
								}else if($specsalgo == '6'){
									$specsalgoout = "Blakecoin";
								}else if($specsalgo == '7'){
									$specsalgoout = "c11";
								}else if($specsalgo == '8'){
									$specsalgoout = "Groestl";
								}else if($specsalgo == '9'){
									$specsalgoout = "Hex";
								}else if($specsalgo == '10'){
									$specsalgoout = "Hmq1725";
								}else if($specsalgo == '11'){
									$specsalgoout = "Keccak";
								}else if($specsalgo == '12'){
									$specsalgoout = "Keccakc";
								}else if($specsalgo == '13'){
									$specsalgoout = "Lbk3";
								}else if($specsalgo == '14'){
									$specsalgoout = "Lbry";
								}else if($specsalgo == '15'){
									$specsalgoout = "Lyra2v2";
								}else if($specsalgo == '16'){
									$specsalgoout = "Lyra2z";
								}else if($specsalgo == '17'){
									$specsalgoout = "M7M";
								}else if($specsalgo == '18'){
									$specsalgoout = "Myr-gr";
								}else if($specsalgo == '19'){
									$specsalgoout = "Neoscrypt";
								}else if($specsalgo == '20'){
									$specsalgoout = "Nist5";
								}else if($specsalgo == '21'){
									$specsalgoout = "Phi";
								}else if($specsalgo == '22'){
									$specsalgoout = "Phi2";
								}else if($specsalgo == '23'){
									$specsalgoout = "Quark";
								}else if($specsalgo == '24'){
									$specsalgoout = "Qubit";
								}else if($specsalgo == '25'){
									$specsalgoout = "Scrypt";
								}else if($specsalgo == '26'){
									$specsalgoout = "Sib";
								}else if($specsalgo == '27'){
									$specsalgoout = "Skein";
								}else if($specsalgo == '28'){
									$specsalgoout = "Skunk";
								}else if($specsalgo == '29'){
									$specsalgoout = "Tribus";
								}else if($specsalgo == '30'){
									$specsalgoout = "x11";
								}else if($specsalgo == '31'){
									$specsalgoout = "x16r";
								}else if($specsalgo == '32'){
									$specsalgoout = "x16s";
								}else if($specsalgo == '33'){
									$specsalgoout = "x17";
								}else if($specsalgo == '34'){
									$specsalgoout = "x22i";
								}else if($specsalgo == '35'){
									$specsalgoout = "Xevan";
								}else if($specsalgo == '36'){
									$specsalgoout = "Yescrypt";
								}	
								if($package == '0') {								
									echo '<div class="main">';
									echo '    <div class="container">';
									echo '        <div class="row">';
									echo '            <div class="col s12">';
									echo "                <center><h4><center><span><img src=images\coins\\" . $logo . " height=50 width=50></span> ". $_POST['symbol'] ." Coin Details</h4></center>";
									echo '            </div>';
									echo '        </div>';
									echo '        <div class="row" style="">									';
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>business</i>FREE Details</div>";
									echo "     <div class=collapsible-body>";
									echo "		<table>";
									echo "		 <thead><tr><th>Coin Name</th><th>Ticker</th><th>Website</th><th>Price</th><th>Percent Change</th></tr></thead>";
									echo "		 <tbody>";
									echo "		  <tr>";
									echo '     		<td>' . $name . '</td>';
									echo '     		<td>' . $symbol . '</td>';
									echo '     		<td><a href="' . $url . '" target="_blank">' . $url . '</a></td>';
									echo '			<td>$' . price($symbol) . ' USD</td>';
									echo '			<td>';
									if (avg($symbol) < 0){
										echo "<p style=color:red;>";
									}else if(avg($symbol) > 0){
										echo "<p style=color:green;>";
									}else{
										echo "<p>";
									}									
									echo '' . avg($symbol) . '';
									echo '%</td>';
									echo "     	  </tr>";
									echo "		</tbody></table>";
									echo "   </li>";
									echo "  </ul>";
								} elseif($package == '1') {
									echo '<div class="main">';
									echo '    <div class="container">';
									echo '        <div class="row">';
									echo '            <div class="col s12">';
									echo "                <center><h4><center><span><img src=images\coins\\" . $logo . " height=50 width=50></span> ". $_POST['symbol'] ." Coin Details</h4></center>";
									echo '            </div>';
									echo '        </div>';
									echo '        <div class="row" style="">									';
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>business</i>BRONZE Details</div>";
									echo "     <div class=collapsible-body>";
									echo "		<table>";
									echo "		 <thead><tr><th>Coin Name</th><th>Ticker</th><th>Website</th><th>Price</th></tr></thead>";
									echo "		 <tbody>";
									echo "		  <tr>";
									echo '     		<td>' . $name . '</td>';
									echo '     		<td>' . $symbol . '</td>';
									echo '     		<td><a href="' . $url . '" target="_blank">' . $url . '</a></td>';
									echo '			<td>$' . price($symbol) . ' USD</td>';									
									echo "     	  </tr>";
									echo "		</tbody></table>";
									echo "   </li>";
									echo "</ul>";									
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>link</i>$name Links</div>";
									echo "     <div class=collapsible-body>";									
									echo "		<table class=highlight>";
									echo "		 <thead><tr><th></th><th></th></tr></thead>";
									echo "		 <tbody>";
									echo '     		<tr><td><img src=images\GitHub-Mark-32px.png >   Github Link</td><td><a href="' . $github . '" target="_blank">' . $github . '</a></td></tr>';									
									echo '     		<tr><td><img src=images\btc-icon.png >   Btctalk Link</td><td><a href="' . $btctalk . '" target="_blank">' . $btctalk . '</a></td></tr>';
									echo '     		<tr><td><img src=images\facebook-icon.png >   Facebook Link</td><td><a href="' . $facebook . '" target="_blank">' . $facebook . '</a></td></tr>';									
									echo '     		<tr><td><img src=images\discord-icon.png >   Discord Link</td><td><a href="' . $discord . '" target="_blank">' . $discord . '</a></td></tr>';
									echo '     		<tr><td><img src=images\telegram-icon.png >   Telegram Link</td><td><a href="' . $telegram . '" target="_blank">' . $telegram . '</a></td></tr>';								
									echo "		</tbody></table>";									
									echo "   </li>";
									echo "</ul>";
								}elseif($package == '2') {
									echo '<div class="main">';
									echo '    <div class="container">';
									echo '        <div class="row">';
									echo '            <div class="col s12">';
									echo "                <center><h4><center><span><img src=images\coins\\" . $logo . " height=50 width=50></span> ". $_POST['symbol'] ." Coin Details</h4></center>";
									echo '            </div>';
									echo '        </div>';
									echo '        <div class="row" style="">									';
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>business</i>SILVER Details</div>";
									echo "     <div class=collapsible-body>";
									echo "		<table>";
									echo "		 <thead><tr><th>Coin Name</th><th>Ticker</th><th>Website</th><th>Price</th></tr></thead>";
									echo "		 <tbody>";
									echo "		  <tr>";
									echo '     		<td>' . $name . '</td>';
									echo '     		<td>' . $symbol . '</td>';
									echo '     		<td><a href="' . $url . '" target="_blank">' . $url . '</a></td>';
									echo '			<td>$' . price($symbol) . ' USD</td>';									
									echo "     	  </tr>";
									echo "		</tbody></table>";
									echo "		<table>";
									echo "		 <thead><tr><th>Coin Description</th></tr></thead>";
									echo "		 <tbody>";
									echo "		  <tr>";
									echo '     		<td>' . $description . '</td>';
									echo "     	  </tr>";
									echo "		</tbody></table>";
									echo "   </li>";
									echo "</ul>";	
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>assessment</i>$name Coins Specs</div>";
									echo "     <div class=collapsible-body>";									
									echo "		<table class=highlight>";
									echo "		 <thead><tr><th>Algo</th><th>POW / POS / MN</th><th>Total Coins Issued</th><th>Block Time</th><th>Block Reward</th><th>Block Maturity</th><th>MasterNode Collateral</th><th>MasterNode Reward</th></tr></thead>";
									echo "		 <tbody>";
									echo '     		<tr><td>' . $specsalgoout . '</td><td>' . $specspowout . '</td><td>' . $totalcoinsissuedout . '</td><td>' . $blocktime . ' seconds</td><td>' . $blockreward . ' coins</td><td>' . $blockmature . ' seconds</td><td>' . $masternodecolatt . ' coins</td><td>' . $maternodereward . '%</td></tr>';									
									echo "		</tbody></table>";									
									echo "   </li>";
									echo "</ul>";									
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>link</i>$name Links</div>";
									echo "     <div class=collapsible-body>";									
									echo "		<table class=highlight>";
									echo "		 <thead><tr><th></th><th></th></tr></thead>";
									echo "		 <tbody>";
									echo '     		<tr><td><img src=images\GitHub-Mark-32px.png >   Github Link</td><td><a href="' . $github . '" target="_blank">' . $github . '</a></td></tr>';									
									echo '     		<tr><td><img src=images\btc-icon.png >   Btctalk Link</td><td><a href="' . $btctalk . '" target="_blank">' . $btctalk . '</a></td></tr>';
									echo '     		<tr><td><img src=images\facebook-icon.png >   Facebook Link</td><td><a href="' . $facebook . '" target="_blank">' . $facebook . '</a></td></tr>';									
									echo '     		<tr><td><img src=images\discord-icon.png >   Discord Link</td><td><a href="' . $discord . '" target="_blank">' . $discord . '</a></td></tr>';
									echo '     		<tr><td><img src=images\telegram-icon.png >   Telegram Link</td><td><a href="' . $telegram . '" target="_blank">' . $telegram . '</a></td></tr>';
									echo '			<tr><td><img src=images\search-icon.png >   Explorer Link</td><td><a href="' . $explorerlink . '" target="_blank">' . $explorerlink . '</a></td></tr>';
									echo '			<tr><td><img src=images\mno.png >   MasterNodes Online Link</td><td><a href="' . $masternodeonlinelink . '" target="_blank">' . $masternodeonlinelink . '</a></td></tr>';									
									echo "		</tbody></table>";									
									echo "   </li>";
									echo "</ul>";									
								}elseif($package == '3') {
									echo '<div class="main">';
									echo '    <div class="container">';
									echo '        <div class="row">';
									echo '            <div class="col s12">';
									echo "                <center><h4><center><span><img src=images\coins\\" . $logo . " height=50 width=50></span> ". $_POST['symbol'] ." Coin Details</h4></center>";
									echo '            </div>';
									echo '        </div>';
									echo '        <div class="row" style="">									';
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>business</i>GOLD Details</div>";
									echo "     <div class=collapsible-body>";
									echo "		<table>";
									echo "		 <thead><tr><th>Coin Name</th><th>Ticker</th><th>Website</th><th>Price</th></tr></thead>";
									echo "		 <tbody>";
									echo "		  <tr>";
									echo '     		<td>' . $name . '</td>';
									echo '     		<td>' . $symbol . '</td>';
									echo '     		<td><a href="' . $url . '" target="_blank">' . $url . '</a></td>';
									echo '			<td>$' . price($symbol) . ' USD</td>';
									echo "     	  </tr>";
									echo "		</tbody></table>";
									echo "		<table>";
									echo "		 <thead><tr><th>Coin Description</th></tr></thead>";
									echo "		 <tbody>";
									echo "		  <tr>";
									echo '     		<td>' . $description . '</td>';
									echo "     	  </tr>";
									echo "		</tbody></table>";
									echo "   </li>";
									echo "</ul>";	
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>assessment</i>$name Coins Specs</div>";
									echo "     <div class=collapsible-body>";									
									echo "		<table class=highlight>";
									echo "		 <thead><tr><th>Algo</th><th>POW / POS / MN</th><th>Total Coins Issued</th><th>Block Time</th><th>Block Reward</th><th>Block Maturity</th><th>MasterNode Collateral</th><th>MasterNode Reward</th></tr></thead>";
									echo "		 <tbody>";
									echo '     		<tr><td>' . $specsalgoout . '</td><td>' . $specspowout . '</td><td>' . $totalcoinsissuedout . '</td><td>' . $blocktime . ' seconds</td><td>' . $blockreward . ' coins</td><td>' . $blockmature . ' seconds</td><td>' . $masternodecolatt . ' coins</td><td>' . $maternodereward . '%</td></tr>';									
									echo "		</tbody></table>";									
									echo "   </li>";
									echo "</ul>";									
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>link</i>$name Links</div>";
									echo "     <div class=collapsible-body>";									
									echo "		<table class=highlight>";
									echo "		 <thead><tr><th></th><th></th></tr></thead>";
									echo "		 <tbody>";
									echo '     		<tr><td><img src=images\GitHub-Mark-32px.png >   Github Link</td><td><a href="' . $github . '" target="_blank">' . $github . '</a></td></tr>';									
									echo '     		<tr><td><img src=images\btc-icon.png >   Btctalk Link</td><td><a href="' . $btctalk . '" target="_blank">' . $btctalk . '</a></td></tr>';
									echo '     		<tr><td><img src=images\facebook-icon.png >   Facebook Link</td><td><a href="' . $facebook . '" target="_blank">' . $facebook . '</a></td></tr>';									
									echo '     		<tr><td><img src=images\discord-icon.png >   Discord Link</td><td><a href="' . $discord . '" target="_blank">' . $discord . '</a></td></tr>';
									echo '     		<tr><td><img src=images\telegram-icon.png >   Telegram Link</td><td><a href="' . $telegram . '" target="_blank">' . $telegram . '</a></td></tr>';
									echo '			<tr><td><img src=images\search-icon.png >   Explorer Link</td><td><a href="' . $explorerlink . '" target="_blank">' . $explorerlink . '</a></td></tr>';
									echo '			<tr><td><img src=images\mno.png >   MasterNodes Online Link</td><td><a href="' . $masternodeonlinelink . '" target="_blank">' . $masternodeonlinelink . '</a></td></tr>';									
									echo "		</tbody></table>";									
									echo "   </li>";
									echo "</ul>";
									echo "<ul class=collapsible>";
									echo "    <li class=active>";
									echo "    <div class=collapsible-header><i class=material-icons>assessment</i>$name Coins Roadmap</div>";
									echo "     <div class=collapsible-body>";	
									echo "		<img src=$roadmap width=100% height=100%>";
									echo "	   </div>";
									echo "   </li>";
									echo "</ul>";									
								} 
							}
						}								
			?>
            </div>
        </div>
    </div>

<?php require_once('footer.php'); ?>