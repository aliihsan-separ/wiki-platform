<?php
require_once('database.php');
require_once('utils.php');

if(!isset($_GET) || !isset($_GET['email']) || empty($_GET['email'])) {
    echo '{"error": true}';
    exit();
}else{
	$email = $mysqli->escape_string(trim($_GET['email']));
	$seed = guidv4(openssl_random_pseudo_bytes(16));

	if($mysqli->query("INSERT INTO users (guid, email) VALUES ('$seed', '$email')")) {
		echo '{"error": false, "seed": "' . $seed . '"}';
	} else {
		echo '{"error": true}';
	}
}


