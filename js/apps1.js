$(function () {
    console.log("ready!");

    $('#modifyButton').click(function () {
        if ($(':radio:checked').length === 0) {
            alert("Please select a coin");
            return false;
        }
    });

    $("input[name=group]").click(function (e) {

        var c = $(this);

        $.ajax({
            url: 'ajax.php',
            type: 'post',
            data: {'coin_id': c.data('id')},
            success: function (data) {

                var coin = JSON.parse(data)[0];
                $('.modal-title').text('Modify ' + coin.name);

                if (coin.package == 0) {

                    document.getElementById("modify-projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                        + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control value='" + coin.project_name + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Ticker for your Project</label>"
                        + "	<input name=ticker id=ticker maxlength=5 type=text required='required' class=form-control value='" + coin.symbol + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                        + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control value='" + coin.url + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "<div class=form-group>"
                        + "<div class='form-group col m6'>"
                        + "  <label class=control-label>Hash Algorithm</label>"
                        + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                        + selected_algo(coin.specsalgo)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                        + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                        + selected_pow(coin.specspow)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group'><label>Upload Logo</label><div class='file-field input-field'><div class='light-blue darken-4 btn'>"
                        + "<span>Browse</span><input type='file' name='myfile' id='fileToUpload'/> <input type = 'hidden' name='TierSel' value='"+coin.package+"' />"
                        + "<input type='hidden' name='id' value='"+coin.coin_id+"'>"
                        + "</div><div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' />"
                        + "</div>";
                } else if (coin.package == 1) {
                    document.getElementById("modify-projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                        + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control value='" + coin.project_name + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Ticker for your Project</label>"
                        + "	<input name=ticker id=ticker maxlength=5 type=text aria-required='true' required='required' class=form-control value='" + coin.symbol + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                        + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control value='" + coin.url + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Github Link</label>"
                        + "	<input name=githublink id=githublink maxlength=200 type=text aria-required='true' required=required class=form-control value='" + coin.github + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Bitcointalk Link</label>"
                        + "	<input name=btctalklink id=btctalklink maxlength=200 type=text class=form-control value='" + coin.btctalk + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Facebook Link</label>"
                        + "	<input name=facebooklink id=facebooklink maxlength=200 type=text class=form-control value='" + coin.facebook + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Twitter Link</label>"
                        + "	<input name=twitterlink id=twitterlink maxlength=200 type=text class=form-control value='" + coin.twitter + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Discord Link</label>"
                        + "	<input name=discordlink id=discordlink maxlength=200 type=text class=form-control value='" + coin.discord + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Telegram Link</label>"
                        + "	<input name=telegramlink id=telegramlink maxlength=200 type=text class=form-control value='" + coin.telegram + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "  <label class=control-label>Hash Algorithm</label>"
                        + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                        + selected_algo(coin.specsalgo)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                        + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                        + selected_pow(coin.specspow)
                        + "  </select>"
                        + "</div>"
                        + "<div class=form-group>"
                        + "<label>Upload Logo</label><div class = 'file-field input-field'><div class = 'light-blue darken-4 btn'>"
                        + "<span>Browse</span><input type = 'file' name='myfile' id='fileToUpload' /><input type = 'hidden' name='TierSel' value='"+coin.package+"' />"
                        + "<input type='hidden' name='id' value='"+coin.coin_id+"'>"
                        + "</div>"
                        + "<div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' /></div>";
                } else if (coin.package == 2) {
                    document.getElementById("modify-projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                        + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control value='" + coin.project_name + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Ticker for your Project</label>"
                        + "	<input name=ticker id=ticker maxlength=5 type=text aria-required='true' required='required' class=form-control value='" + coin.symbol + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label >What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                        + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control value='" + coin.url + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Github Link</label>"
                        + "	<input name=githublink id=githublink maxlength=200 type=text aria-required='true' required=required class=form-control value='" + coin.github + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Bitcointalk Link</label>"
                        + "	<input name=btctalklink id=btctalklink maxlength=200 type=text class=form-control value='" + coin.btctalk + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Facebook Link</label>"
                        + "	<input name=facebooklink id=facebooklink maxlength=200 type=text class=form-control value='" + coin.facebook + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Twitter Link</label>"
                        + "	<input name=twitterlink id=twitterlink maxlength=200 type=text class=form-control value='" + coin.twitter + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Discord Link</label>"
                        + "	<input name=discordlink id=discordlink maxlength=200 type=text class=form-control value='" + coin.discord + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Telegram Link</label>"
                        + "	<input name=telegramlink id=telegramlink maxlength=200 type=text class=form-control value='" + coin.telegram + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Description of your project</label>"
                        + "<textarea id=description name=description class='materialize-textarea' value='" + coin.description + "' ></textarea>"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What are the Coin Specs</label>"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "  <label class=control-label>Hash Algorithm</label>"
                        + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                        + selected_algo(coin.specsalgo)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                        + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                        + selected_pow(coin.specspow)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Total Coins Issued</label>"
                        + "	<input name=totalcoinsissued id=totalcoinsissued maxlength=200 type=text class=form-control value='" + coin.totalcoinsissued + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Block Time in Seconds</label>"
                        + "	<input name=blocktime id=blocktime maxlength=200 type=text class=form-control value='" + coin.blocktime + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Block Reward in Coins</label>"
                        + "	<input name=blockreward id=blockreward maxlength=200 type=text class=form-control value='" + coin.blockreward + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Block Mature in Seconds</label>"
                        + "	<input name=blockmature id=blockmature maxlength=200 type=text class=form-control value='" + coin.blockmature + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is Masternode Online Link</label>"
                        + "	<input name=masternodeonlinelink id=masternodeonlinelink maxlength=200 type=text class=form-control value='" + coin.mno + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is Masternode Colateral and Rewards</label>"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>MasterNode Collateral Required</label>"
                        + "	<input name=masternodecolatt id=masternodecolatt maxlength=200 type=text class=form-control value='" + coin.masternodecolatt + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>MasterNode Rewards</label>"
                        + "	<input name=maternodereward id=maternodereward maxlength=200 type=text class=form-control value='" + coin.maternodereward + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Explorer Link</label>"
                        + "	<input name=explorerlink id=explorerlink maxlength=200 type=text class=form-control value='" + coin.explorer + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "<div class=form-group>"
                        + "<label>Upload Logo</label><div class = 'file-field input-field'><div class = 'light-blue darken-4 btn'>"
                        + "<span>Browse</span><input type = 'file' name='myfile' id='fileToUpload' /> <input type = 'hidden' name='TierSel' value='"+coin.package+"' />"
                        + "<input type='hidden' name='id' value='"+coin.coin_id+"'>"
                        + "</div>"
                        + "<div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' /></div>";
                } else if (coin.package == 3) {
                    document.getElementById("modify-projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                        + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control value='" + coin.project_name + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Ticker for your Project</label>"
                        + "	<input name=ticker id=ticker maxlength=5 type=text aria-required='true' required='required' class=form-control value='" + coin.symbol + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label >What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                        + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control value='" + coin.url + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Github Link</label>"
                        + "	<input name=githublink id=githublink maxlength=200 type=text aria-required='true' required=required class=form-control value='" + coin.github + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Bitcointalk Link</label>"
                        + "	<input name=btctalklink id=btctalklink maxlength=200 type=text class=form-control value='" + coin.btctalk + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Facebook Link</label>"
                        + "	<input name=facebooklink id=facebooklink maxlength=200 type=text class=form-control value='" + coin.facebook + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Twitter Link</label>"
                        + "	<input name=twitterlink id=twitterlink maxlength=200 type=text class=form-control value='" + coin.twitter + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group  col m6'>"
                        + "	<label class=control-label>What is the Discord Link</label>"
                        + "	<input name=discordlink id=discordlink maxlength=200 type=text class=form-control value='" + coin.discord + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Telegram Link</label>"
                        + "	<input name=telegramlink id=telegramlink maxlength=200 type=text class=form-control value='" + coin.telegram + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Description of your project</label>"
                        + "<textarea id=description name=description class='materialize-textarea' value='" + coin.description + "' ></textarea>"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What are the Coin Specs</label>"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "  <label class=control-label>Hash Algorithm</label>"
                        + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                        + selected_algo(coin.specsalgo)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                        + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                        + selected_pow(coin.specspow)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Total Coins Issued</label>"
                        + "	<input name=totalcoinsissued id=totalcoinsissued maxlength=200 type=text class=form-control value='" + coin.totalcoinsissued + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Block Time in Seconds</label>"
                        + "	<input name=blocktime id=blocktime maxlength=200 type=text class=form-control value='" + coin.blocktime + "' pattern='^[a-zA-Z0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Block Reward in Coins</label>"
                        + "	<input name=blockreward id=blockreward maxlength=200 type=text class=form-control value='" + coin.blockreward + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m2'>"
                        + "	<label class=control-label>Block Mature in Seconds</label>"
                        + "	<input name=blockmature id=blockmature maxlength=200 type=text class=form-control value='" + coin.blockmature + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is Masternode Online Link</label>"
                        + "	<input name=masternodeonlinelink id=masternodeonlinelink maxlength=200 type=text class=form-control value='" + coin.mno + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is Masternode Colateral and Rewards</label>"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>MasterNode Collateral Required</label>"
                        + "	<input name=masternodecolatt id=masternodecolatt maxlength=200 type=text class=form-control value='" + coin.masternodecolatt + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>MasterNode Rewards</label>"
                        + "	<input name=maternodereward id=maternodereward maxlength=200 type=text class=form-control value='" + coin.maternodereward + "' pattern='^[0-9\\s]+' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "	<label class=control-label>What is the Explorer Link</label>"
                        + "	<input name=explorerlink id=explorerlink maxlength=200 type=text class=form-control value='" + coin.explorer + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group'>"
                        + "  <label class=control-label>What Exchanges Listed On</label>"
                        + "  <select class=browser-default id=exchange name=exchange><option value='' disabled selected>Choose your option</option>"
                        + selected_exchange(coin.exchanges)
                        + "  </select>"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>What is the KYD Link</label>"
                        + "	<input name=kydlink id=kydlink maxlength=200 type=text class=form-control value='" + coin.kyd + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>What is the Whitepaper Link</label>"
                        + "	<input name=whitepaperlink id=whitepaperlink maxlength=200 type=text class=form-control value='" + coin.whitepaper + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>What is the Roadmap Link</label>"
                        + "	<input name=roadmaplink id=roadmaplink maxlength=200 type=text class=form-control value='" + coin.roadmap + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>What is the Launch Date</label>"
                        + "	<input name=launchdate id=launchdate maxlength=200 type=text class=form-control value='" + coin.launch + "' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>What is the Pre-Sale Link</label>"
                        + "	<input name=presalelink id=presalelink maxlength=200 type=text class=form-control value='" + coin.presale + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class='form-group col m6'>"
                        + "	<label class=control-label>What is the ROI Calculator Link</label>"
                        + "	<input name=roilink id=roilink maxlength=200 type=text class=form-control value='" + coin.roi + "' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                        + "</div>"
                        + "<div class=form-group>"
                        + "<label>Upload Logo</label><div class = 'file-field input-field'><div class = 'light-blue darken-4 btn'>"
                        + "<span>Browse</span><input type = 'file' name='myfile' id='fileToUpload' /><input type = 'hidden' name='TierSel' value='"+coin.package+"' />"
                        + "</div>"
                        + "<div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' /></div>";
                }
            }
        });

    });
});

function selected_algo(specsalgo) {
    var options = ["aergo", "allium", "bcd", "bitcore", "blake2s", "blakecoin", "c11", "groestl", "hex", "hmq1725", "keccak", "keccakc", "lbk3", "lbry", "lyra2v2", "lyra2z", "m7m", "myr-gr", "neoscrypt", "nist5", "phi", "phi2", "quark", "qubit", "scrypt", "sib", "skein", "skunk", "tribus", "x11", "x16r", "x16s", "x17", "x22i", "xevan", "yescrypt", "sha256"];
    var option = '';
    for (let i = 0; i < options.length; i++) {
        if (specsalgo == i) {
            option += '<option value="' + i + '" selected>' + options[i] + ' </option>';
        } else {
            option += '<option value="' + i + '">' + options[i] + ' </option>';
        }
    }
    return option;
}

function selected_pow(specspow) {
    var options = ["POW", "POS", "POW & POS", "POW & MN", "POS & MN"];
    var option = '';
    for (let i = 0; i < options.length; i++) {
        if (specspow == i) {
            option += '<option value="' + i + '" selected>' + options[i] + ' </option>';
        } else {
            option += '<option value="' + i + '">' + options[i] + ' </option>';
        }
    }
    return option;
}

function selected_exchange(exchanges) {
    var options = ["Crypto Bridge", "Cryptopia", "OKEx", "Binance", "Huobi", "DigiFinex", "ZB.COM", "Bitfinex", "CoinBene", "HitBTC", "Bibox", "DOBI Trade", "LBank", "IDAX",
        "BCEX", "UPBIT", "IDCM", "BitMart", "Bit-Z", "Kraken", "Bittrex", "Kucoin", "Poloniex", "YoBit", "Crex24", "BiteBTC"];
    var option = '';
    for (let i = 0; i < options.length; i++) {
        if (exchanges == i) {
            option += '<option value="' + i + '" selected>' + options[i] + ' </option>';
        } else {
            option += '<option value="' + i + '">' + options[i] + ' </option>';
        }
    }
    return option;
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function instantValidation(field) {
    if (shouldBeValidated(field)) {
        var invalid =
            (field.getAttribute("required") && !field.value) ||
            (field.getAttribute("pattern") &&
                field.value &&
                !new RegExp(field.getAttribute("pattern")).test(field.value));
        if (!invalid && field.getAttribute("aria-invalid")) {
            field.removeAttribute("aria-invalid");
        } else if (invalid && !field.getAttribute("aria-invalid")) {
            field.setAttribute("aria-invalid", "true");
        }
    }
}

function shouldBeValidated(field) {
    return (
        !(field.getAttribute("readonly") || field.readonly) &&
        !(field.getAttribute("disabled") || field.disabled) &&
        (field.getAttribute("pattern") || field.getAttribute("required"))
    );
}

function addEvent(node, type, callback) {
    if (node.addEventListener) {
        node.addEventListener(type, function (e) {
            callback(e, e.target);
        }, false);
    } else if (node.attachEvent) {
        node.attachEvent('on' + type, function (e) {
            callback(e, e.srcElement);
        });
    }
}

addEvent(document, "change", function (e, target) {
    instantValidation(target);
});

var fields = [
    document.getElementsByTagName("input"),
    document.getElementsByTagName("textarea")
];
for (var a = fields.length, i = 0; i < a; i++) {
    for (var b = fields[i].length, j = 0; j < b; j++) {
        addEvent(fields[i][j], "change", function (e, target) {
            instantValidation(target);
        });
    }
}


$(document).ready(function () {
    $('.modal').modal();

    $('.sidenav').sidenav();
    $('.collapsible').collapsible();

    $('#register').on('submit', function (e) {
        e.preventDefault();
        var email = $.trim($('#email').val().toLowerCase());
        if (!validateEmail(email)) {
            return M.toast({html: 'Invalid email address.'});
        }
        $.getJSON("regAPI.php?email=" + email, function (res) {
            console.log(res);
            if (res.error) {
                M.toast({html: 'Account already registered.'});
            } else {
                $('#seedRes').text(res.seed);
                $('#regModal').modal('open');
            }
        });
    });

    $('#blockchain-select').on('change', function (e) {
        e.preventDefault();
        $('.coin-info').hide();
        $('#' + ($('#blockchain-select').val())).show();
    });

    $('.next').click(function () {
        var nextId = $(this).parents('.tab-pane').next().attr("id");
        $('[href=#' + nextId + ']').tab('show');

        return false;
    })

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        //update progress
        var step = $(e.target).data('step');
        var percent = (parseInt(step) / 5) * 100;

        $('.progress-bar').css({width: percent + '%'});
        $('.progress-bar').text("Step " + step + " of 5");

        //e.relatedTarget // previous tab

    })

    $('.first').click(function () {
        $('#myWizard a:first').tab('show')
    })

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

function ChangecatList1() {
    var myShows = ['WesaHVy7aXkvhgjHVReVLsug8qFhcqT1f4', 'WRCjhMZjesR61bfKDCpKPba739mVsLqwQc', 'Wi6eNQx1kuPoLR2S9aL5maFypsfzewYQGo'];
    var show = myShows[Math.floor(Math.random() * myShows.length)];

    selval = document.getElementById("TierSel").value;
    seltier = document.getElementById("feat").value;
    if (selval) {
        if (selval == 0) {
            if (seltier == 0) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>FREE Package no payment required.</label><input name='txid' id='txid' type='hidden' value='0' />";
            } else if (seltier == 1) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 100 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 2) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 73 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 37 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 5000 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            }
        } else if (selval == 1) {
            if (seltier == 0) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 90 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 1) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 190 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 2) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 163 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 127 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 5000 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            }
        } else if (selval == 2) {
            if (seltier == 0) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 137 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 1) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 237 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 2) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 210 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 174 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 5000 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            }
        } else if (selval == 3) {
            if (seltier == 0) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 182 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 1) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 282 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 2) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 255 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 219 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            } else if (seltier == 3) {
                document.getElementById("paymentinfo").innerHTML = "<label class='control-label'>Please pay 5000 WikiCoins to " + show + " </label><input name='txid' id='txid' maxlength='100' type='text' required='required' class='form-control' placeholder='Enter TXID' pattern='^[a-zA-Z0-9\\s]+' />";
            }
        }
    }
}

function ChangecatList() {
    selval = document.getElementById("TierSel").value;
    if (selval) {
        if (selval == 0) {
            document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control placeholder='Enter Project Name' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Ticker for your Project</label>"
                + "	<input name=ticker id=ticker maxlength=5 type=text required='required' class=form-control placeholder='Enter Ticker' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control placeholder='Enter Website URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "<div class=form-group>"
                + "<div class='form-group col m6'>"
                + "  <label class=control-label>Hash Algorithm</label>"
                + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>aergo</option>"
                + "     <option value=2>allium</option>"
                + "     <option value=3>bcd</option>"
                + "     <option value=4>bitcore</option>"
                + "     <option value=5>blake2s</option>"
                + "     <option value=6>blakecoin</option>"
                + "     <option value=7>c11</option>"
                + "     <option value=8>groestl</option>"
                + "     <option value=9>hex</option>"
                + "     <option value=10>hmq1725</option>"
                + "     <option value=11>keccak</option>"
                + "     <option value=12>keccakc</option>"
                + "     <option value=13>lbk3</option>"
                + "     <option value=14>lbry</option>"
                + "     <option value=15>lyra2v2</option>"
                + "     <option value=16>lyra2z</option>"
                + "     <option value=17>m7m</option>"
                + "     <option value=18>myr-gr</option>"
                + "     <option value=19>neoscrypt</option>"
                + "     <option value=20>nist5</option>"
                + "     <option value=21>phi</option>"
                + "     <option value=22>phi2</option>"
                + "     <option value=23>quark</option>"
                + "     <option value=24>qubit</option>"
                + "     <option value=25>scrypt</option>"
                + "     <option value=26>sib</option>"
                + "     <option value=27>skein</option>"
                + "     <option value=28>skunk</option>"
                + "     <option value=29>tribus</option>"
                + "     <option value=30>x11</option>"
                + "     <option value=31>x16r</option>"
                + "     <option value=32>x16s</option>"
                + "     <option value=33>x17</option>"
                + "     <option value=34>x22i</option>"
                + "     <option value=35>xevan</option>"
                + "     <option value=36>yescrypt</option>"
                + "     <option value=37>sha256</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>POW</option>"
                + "     <option value=2>POS</option>"
                + "     <option value=3>POW & POS</option>"
                + "     <option value=4>POW & MN</option>"
                + "     <option value=5>POS & MN</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group'><label>Upload Logo</label><div class='file-field input-field'><div class='light-blue darken-4 btn'>"
                + "<span>Browse</span><input type='file' name='myfile' id='fileToUpload' required='required'/>"
                + "</div><div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' />"
                + "</div>";
        } else if (selval == 1) {
            document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control placeholder='Enter Project Name' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Ticker for your Project</label>"
                + "	<input name=ticker id=ticker maxlength=5 type=text aria-required='true' required='required' class=form-control placeholder='Enter Ticker' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control placeholder='Enter Website URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Github Link</label>"
                + "	<input name=githublink id=githublink maxlength=200 type=text aria-required='true' required=required class=form-control placeholder='Enter Github URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Bitcointalk Link</label>"
                + "	<input name=btctalklink id=btctalklink maxlength=200 type=text class=form-control placeholder='Enter Bitcoin Talk URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Facebook Link</label>"
                + "	<input name=facebooklink id=facebooklink maxlength=200 type=text class=form-control placeholder='Enter Facebook URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Twitter Link</label>"
                + "	<input name=twitterlink id=twitterlink maxlength=200 type=text class=form-control placeholder='Enter Twitter URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Discord Link</label>"
                + "	<input name=discordlink id=discordlink maxlength=200 type=text class=form-control placeholder='Enter Discord Permanant Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Telegram Link</label>"
                + "	<input name=telegramlink id=telegramlink maxlength=200 type=text class=form-control placeholder='Enter Telegram Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "  <label class=control-label>Hash Algorithm</label>"
                + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>aergo</option>"
                + "     <option value=2>allium</option>"
                + "     <option value=3>bcd</option>"
                + "     <option value=4>bitcore</option>"
                + "     <option value=5>blake2s</option>"
                + "     <option value=6>blakecoin</option>"
                + "     <option value=7>c11</option>"
                + "     <option value=8>groestl</option>"
                + "     <option value=9>hex</option>"
                + "     <option value=10>hmq1725</option>"
                + "     <option value=11>keccak</option>"
                + "     <option value=12>keccakc</option>"
                + "     <option value=13>lbk3</option>"
                + "     <option value=14>lbry</option>"
                + "     <option value=15>lyra2v2</option>"
                + "     <option value=16>lyra2z</option>"
                + "     <option value=17>m7m</option>"
                + "     <option value=18>myr-gr</option>"
                + "     <option value=19>neoscrypt</option>"
                + "     <option value=20>nist5</option>"
                + "     <option value=21>phi</option>"
                + "     <option value=22>phi2</option>"
                + "     <option value=23>quark</option>"
                + "     <option value=24>qubit</option>"
                + "     <option value=25>scrypt</option>"
                + "     <option value=26>sib</option>"
                + "     <option value=27>skein</option>"
                + "     <option value=28>skunk</option>"
                + "     <option value=29>tribus</option>"
                + "     <option value=30>x11</option>"
                + "     <option value=31>x16r</option>"
                + "     <option value=32>x16s</option>"
                + "     <option value=33>x17</option>"
                + "     <option value=34>x22i</option>"
                + "     <option value=35>xevan</option>"
                + "     <option value=36>yescrypt</option>"
                + "     <option value=37>sha256</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>POW</option>"
                + "     <option value=2>POS</option>"
                + "     <option value=3>POW & POS</option>"
                + "     <option value=4>POW & MN</option>"
                + "     <option value=5>POS & MN</option>"
                + "  </select>"
                + "</div>"
                + "<div class=form-group>"
                + "<label>Upload Logo</label><div class = 'file-field input-field'><div class = 'light-blue darken-4 btn'>"
                + "<span>Browse</span><input type = 'file' name='myfile' id='fileToUpload' required='required'/>"
                + "</div>"
                + "<div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' /></div>";
        } else if (selval == 2) {
            document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control placeholder='Enter Project Name' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Ticker for your Project</label>"
                + "	<input name=ticker id=ticker maxlength=5 type=text aria-required='true' required='required' class=form-control placeholder='Enter Ticker' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label >What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control placeholder='Enter Website URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Github Link</label>"
                + "	<input name=githublink id=githublink maxlength=200 type=text aria-required='true' required=required class=form-control placeholder='Enter Github URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Bitcointalk Link</label>"
                + "	<input name=btctalklink id=btctalklink maxlength=200 type=text class=form-control placeholder='Enter Bitcoin Talk URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Facebook Link</label>"
                + "	<input name=facebooklink id=facebooklink maxlength=200 type=text class=form-control placeholder='Enter Facebook URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Twitter Link</label>"
                + "	<input name=twitterlink id=twitterlink maxlength=200 type=text class=form-control placeholder='Enter Twitter URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Discord Link</label>"
                + "	<input name=discordlink id=discordlink maxlength=200 type=text class=form-control placeholder='Enter Discord Permanant Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Telegram Link</label>"
                + "	<input name=telegramlink id=telegramlink maxlength=200 type=text class=form-control placeholder='Enter Telegram Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Description of your project</label>"
                + "<textarea id=description name=description class='materialize-textarea' placeholder='Enter Description' ></textarea>"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What are the Coin Specs</label>"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "  <label class=control-label>Hash Algorithm</label>"
                + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>aergo</option>"
                + "     <option value=2>allium</option>"
                + "     <option value=3>bcd</option>"
                + "     <option value=4>bitcore</option>"
                + "     <option value=5>blake2s</option>"
                + "     <option value=6>blakecoin</option>"
                + "     <option value=7>c11</option>"
                + "     <option value=8>groestl</option>"
                + "     <option value=9>hex</option>"
                + "     <option value=10>hmq1725</option>"
                + "     <option value=11>keccak</option>"
                + "     <option value=12>keccakc</option>"
                + "     <option value=13>lbk3</option>"
                + "     <option value=14>lbry</option>"
                + "     <option value=15>lyra2v2</option>"
                + "     <option value=16>lyra2z</option>"
                + "     <option value=17>m7m</option>"
                + "     <option value=18>myr-gr</option>"
                + "     <option value=19>neoscrypt</option>"
                + "     <option value=20>nist5</option>"
                + "     <option value=21>phi</option>"
                + "     <option value=22>phi2</option>"
                + "     <option value=23>quark</option>"
                + "     <option value=24>qubit</option>"
                + "     <option value=25>scrypt</option>"
                + "     <option value=26>sib</option>"
                + "     <option value=27>skein</option>"
                + "     <option value=28>skunk</option>"
                + "     <option value=29>tribus</option>"
                + "     <option value=30>x11</option>"
                + "     <option value=31>x16r</option>"
                + "     <option value=32>x16s</option>"
                + "     <option value=33>x17</option>"
                + "     <option value=34>x22i</option>"
                + "     <option value=35>xevan</option>"
                + "     <option value=36>yescrypt</option>"
                + "     <option value=37>sha256</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>POW</option>"
                + "     <option value=2>POS</option>"
                + "     <option value=3>POW & POS</option>"
                + "     <option value=4>POW & MN</option>"
                + "     <option value=5>POS & MN</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Total Coins Issued</label>"
                + "	<input name=totalcoinsissued id=totalcoinsissued maxlength=200 type=text class=form-control placeholder='Total Coins issued' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Block Time in Seconds</label>"
                + "	<input name=blocktime id=blocktime maxlength=200 type=text class=form-control placeholder='Block Time in Seconds' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Block Reward in Coins</label>"
                + "	<input name=blockreward id=blockreward maxlength=200 type=text class=form-control placeholder='Block Reward in Coins' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Block Mature in Seconds</label>"
                + "	<input name=blockmature id=blockmature maxlength=200 type=text class=form-control placeholder='Block Mature in Seconds' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is Masternode Online Link</label>"
                + "	<input name=masternodeonlinelink id=masternodeonlinelink maxlength=200 type=text class=form-control placeholder='Enter Masternodes online Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "<div class=form-group>"
                + "	<label class=control-label>What is Masternode Colateral and Rewards</label>"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>MasterNode Collateral Required</label>"
                + "	<input name=masternodecolatt id=masternodecolatt maxlength=200 type=text class=form-control placeholder='Masternode Collateral Required' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>MasterNode Rewards</label>"
                + "	<input name=maternodereward id=maternodereward maxlength=200 type=text class=form-control placeholder='MasterNode Reward in Percent' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Explorer Link</label>"
                + "	<input name=explorerlink id=explorerlink maxlength=200 type=text class=form-control placeholder='Enter Explorer Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "<div class=form-group>"
                + "<label>Upload Logo</label><div class = 'file-field input-field'><div class = 'light-blue darken-4 btn'>"
                + "<span>Browse</span><input type = 'file' name='myfile' id='fileToUpload' required='required'/>"
                + "</div>"
                + "<div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' /></div>";
        } else if (selval == 3) {
            document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
                + "	<input name=projname id=projname maxlength=200 spellcheck=false type=text aria-required='true' required='required' class=form-control placeholder='Enter Project Name' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Ticker for your Project</label>"
                + "	<input name=ticker id=ticker maxlength=5 type=text aria-required='true' required='required' class=form-control placeholder='Enter Ticker' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the URL for your Project 'eg. http://www.altcoinwiki.com'</label>"
                + "	<input name=website id=website size=30 spellcheck=false type=url aria-required='true' required='required' class=form-control placeholder='Enter Website URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Github Link</label>"
                + "	<input name=githublink id=githublink maxlength=200 type=text aria-required='true' required=required class=form-control placeholder='Enter Github URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Bitcointalk Link</label>"
                + "	<input name=btctalklink id=btctalklink maxlength=200 type=text class=form-control placeholder='Enter Bitcoin Talk URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Facebook Link</label>"
                + "	<input name=facebooklink id=facebooklink maxlength=200 type=text class=form-control placeholder='Enter Facebook URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Twitter Link</label>"
                + "	<input name=twitterlink id=twitterlink maxlength=200 type=text class=form-control placeholder='Enter Twitter URL' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group  col m6'>"
                + "	<label class=control-label>What is the Discord Link</label>"
                + "	<input name=discordlink id=discordlink maxlength=200 type=text class=form-control placeholder='Enter Discord Permanant Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Telegram Link</label>"
                + "	<input name=telegramlink id=telegramlink maxlength=200 type=text class=form-control placeholder='Enter Telegram Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Description of your project</label>"
                + "<textarea id=description name=description class='materialize-textarea' placeholder='Enter Description' ></textarea>"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What are the Coin Specs</label>"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "  <label class=control-label>Hash Algorithm</label>"
                + "  <select class=browser-default id=specsalgo name=specsalgo ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>aergo</option>"
                + "     <option value=2>allium</option>"
                + "     <option value=3>bcd</option>"
                + "     <option value=4>bitcore</option>"
                + "     <option value=5>blake2s</option>"
                + "     <option value=6>blakecoin</option>"
                + "     <option value=7>c11</option>"
                + "     <option value=8>groestl</option>"
                + "     <option value=9>hex</option>"
                + "     <option value=10>hmq1725</option>"
                + "     <option value=11>keccak</option>"
                + "     <option value=12>keccakc</option>"
                + "     <option value=13>lbk3</option>"
                + "     <option value=14>lbry</option>"
                + "     <option value=15>lyra2v2</option>"
                + "     <option value=16>lyra2z</option>"
                + "     <option value=17>m7m</option>"
                + "     <option value=18>myr-gr</option>"
                + "     <option value=19>neoscrypt</option>"
                + "     <option value=20>nist5</option>"
                + "     <option value=21>phi</option>"
                + "     <option value=22>phi2</option>"
                + "     <option value=23>quark</option>"
                + "     <option value=24>qubit</option>"
                + "     <option value=25>scrypt</option>"
                + "     <option value=26>sib</option>"
                + "     <option value=27>skein</option>"
                + "     <option value=28>skunk</option>"
                + "     <option value=29>tribus</option>"
                + "     <option value=30>x11</option>"
                + "     <option value=31>x16r</option>"
                + "     <option value=32>x16s</option>"
                + "     <option value=33>x17</option>"
                + "     <option value=34>x22i</option>"
                + "     <option value=35>xevan</option>"
                + "     <option value=36>yescrypt</option>"
                + "     <option value=37>sha256</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "  <label class=control-label>Proof-Of-Work Scheme</label>"
                + "  <select class=browser-default id=specspow name=specspow ><option value='' disabled selected>Choose your option</option>"
                + "     <option value=1>POW</option>"
                + "     <option value=2>POS</option>"
                + "     <option value=3>POW & POS</option>"
                + "     <option value=4>POW & MN</option>"
                + "     <option value=5>POS & MN</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Total Coins Issued</label>"
                + "	<input name=totalcoinsissued id=totalcoinsissued maxlength=200 type=text class=form-control placeholder='Total Coins issued' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Block Time in seconds</label>"
                + "	<input name=blocktime id=blocktime maxlength=200 type=text class=form-control placeholder='Block Time in Seconds' pattern='^[a-zA-Z0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Block Reward</label>"
                + "	<input name=blockreward id=blockreward maxlength=200 type=text class=form-control placeholder='Amount of Coins' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m2'>"
                + "	<label class=control-label>Block Mature in seconds</label>"
                + "	<input name=blockmature id=blockmature maxlength=200 type=text class=form-control placeholder='Block Mature in seconds' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "	<label class=control-label>What is Masternode Online Link</label>"
                + "	<input name=masternodeonlinelink id=masternodeonlinelink maxlength=200 type=text class=form-control placeholder='Enter Masternodes online Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "<div class=form-group>"
                + "	<label class=control-label>What is Masternode Colateral and Rewards</label>"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>MasterNode Collateral Required</label>"
                + "	<input name=masternodecolatt id=masternodecolatt maxlength=200 type=text class=form-control placeholder='Masternode Collateral Required' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>MasterNode Reward</label>"
                + "	<input name=maternodereward id=maternodereward maxlength=200 type=text class=form-control placeholder='MasterNode Reward in Percent' pattern='^[0-9\\s]+' />"
                + "</div>"
                + "<div class=form-group>"
                + "	<label class=control-label>What is the Explorer Link</label>"
                + "	<input name=explorerlink id=explorerlink maxlength=200 type=text class=form-control placeholder='Enter Explorer Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group'>"
                + "  <label class=control-label>What Exchanges Listed On</label>"
                + "  <select class=browser-default id=exchange name=exchange><option value='' disabled selected>Choose your option</option>"
                + "     <option value=0>Crypto Bridge</option>"
                + "     <option value=1>Cryptopia</option>"
                + "     <option value=2>OKEx</option>"
                + "     <option value=3>Binance</option>"
                + "     <option value=4>Huobi</option>"
                + "     <option value=5>DigiFinex</option>"
                + "     <option value=6>ZB.COM</option>"
                + "     <option value=7>Bitfinex</option>"
                + "     <option value=8>CoinBene</option>"
                + "     <option value=9>HitBTC</option>"
                + "     <option value=10>Bibox</option>"
                + "     <option value=11>DOBI Trade</option>"
                + "     <option value=12>LBank</option>"
                + "     <option value=13>IDAX</option>"
                + "     <option value=14>BCEX</option>"
                + "     <option value=15>UPBIT</option>"
                + "     <option value=16>IDCM</option>"
                + "     <option value=17>BitMart</option>"
                + "     <option value=18>Bit-Z</option>"
                + "     <option value=19>Kraken</option>"
                + "     <option value=20>Bittrex</option>"
                + "     <option value=21>Kucoin</option>"
                + "     <option value=22>Poloniex</option>"
                + "     <option value=23>YoBit</option>"
                + "     <option value=24>Crex24</option>"
                + "     <option value=25>BiteBTC</option>"
                + "  </select>"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>What is the KYD Link</label>"
                + "	<input name=kydlink id=kydlink maxlength=200 type=text class=form-control placeholder='Enter KYD Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>What is the Whitepaper Link</label>"
                + "	<input name=whitepaperlink id=whitepaperlink maxlength=200 type=text class=form-control placeholder='Enter whitepaper Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>What is the Roadmap Link</label>"
                + "	<input name=roadmaplink id=roadmaplink maxlength=200 type=text class=form-control placeholder='Enter Roadmap Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>What is the Launch Date</label>"
                + "	<input name=launchdate id=launchdate maxlength=200 type=text class=form-control placeholder=Enter Website URL />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>What is the Pre-Sale Link</label>"
                + "	<input name=presalelink id=presalelink maxlength=200 type=text class=form-control placeholder='Enter Pre-Sale Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class='form-group col m6'>"
                + "	<label class=control-label>What is the ROI Calculator Link</label>"
                + "	<input name=roilink id=roilink maxlength=200 type=text class=form-control placeholder='Enter ROI Calculator Link' pattern='^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$' />"
                + "</div>"
                + "<div class=form-group>"
                + "<label>Upload Logo</label><div class = 'file-field input-field'><div class = 'light-blue darken-4 btn'>"
                + "<span>Browse</span><input type = 'file' name='myfile' id='fileToUpload' required='required'/>"
                + "</div>"
                + "<div class='file-path-wrapper'><input class='file-path validate' type='text' placeholder='Upload file' /></div>";
        }

    } else {
        alert("bad");
    }
}





