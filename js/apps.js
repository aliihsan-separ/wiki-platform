function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function is_url()
{
	urlval = document.getElementById("website").value;
	regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (regexp.test(urlval))
        {
          return true;
        }
        else
        {
          return false;
        }
}
$(document).ready(function () {
    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
    $('.modal').modal();
    $('#register').on('submit', function (e) {
        e.preventDefault();
        var email = $.trim($('#email').val().toLowerCase());
        if (!validateEmail(email)) {
            return M.toast({ html: 'Invalid email address.' });
        }
        $.getJSON("regAPI.php?email=" + email, function (res) {
            console.log(res);
            if (res.error) {
                M.toast({ html: 'Account already registered.' });
            }
            else {
                $('#seedRes').text(res.seed);
                $('#regModal').modal('open');
            }
        });
    });
    $('#blockchain-select').on('change', function (e) {
        e.preventDefault();
        $('.coin-info').hide();
        $('#' + ($('#blockchain-select').val())).show();
    });
	$('.next').click(function(){
		//test
		
		
		
		
		
		
		
		//end test
		var nextId = $(this).parents('.tab-pane').next().attr("id");
		$('[href=#'+nextId+']').tab('show');
		return false;
	})
  
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
   
		//update progress
		var step = $(e.target).data('step');
		var percent = (parseInt(step) / 4) * 100;
    
		$('.progress-bar').css({width: percent + '%'});
		$('.progress-bar').text("Step " + step + " of 4");
    
		//e.relatedTarget // previous tab
    
	})
  
	$('.first').click(function(){
  		$('#myWizard a:first').tab('show')
  	})
	
	    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

function ChangecatList() {
	selval = document.getElementById("TierSel").value;
	if(selval){
		if (selval == 0){
			document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>"
														   + "	<input name=projname id=projname maxlength=200 type=text required=required class=form-control placeholder=Enter Project Name />"
														   + "</div>"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Ticker for your Project</label>"
														   + "	<input name=ticker id=ticker maxlength=5 type=text required=required class=form-control placeholder=Enter Ticker />"
														   + "</div>"
														   + "<div class=form-group"
														   + "	<label class=control-label>What is the URL for your Project</label>"
														   + "	<input name=website id=website maxlength=200 type=text required=required class=form-control placeholder='Enter Website URL' onchange=is_url()/>";												   		
		}else if(selval == 1){
			document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>" 
														   + "	<input name=projname id=projname maxlength=200 type=text required=required class=form-control placeholder=Enter Project Name />" 
														   + "</div>" 
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Ticker for your Project</label>"
														   + "	<input name=ticker id=ticker maxlength=5 type=text required=required class=form-control placeholder=Enter Ticker />"
														   + "</div>" 	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the URL for your Project</label>"
														   + "	<input name=website id=website maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Github Link</label>"
														   + "	<input name=githublink id=githublink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Bitcoin Talk Link</label>"
														   + "	<input name=btctalklink id=btctalklink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Facebook Link</label>"
														   + "	<input name=facebooklink id=facebooklink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Twitter Link</label>"
														   + "	<input name=twitterlink id=twitterlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Discod Link</label>"
														   + "	<input name=discordlink id=discordlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Telegram Link</label>"
														   + "	<input name=telegramlink id=telegramlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />";
		}else if(selval == 2){
			document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>" 
														   + "	<input name=projname id=projname maxlength=200 type=text required=required class=form-control placeholder=Enter Project Name />" 
														   + "</div>" 
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Ticker for your Project</label>"
														   + "	<input name=ticker id=ticker maxlength=5 type=text required=required class=form-control placeholder=Enter Ticker />"
														   + "</div>" 	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the URL for your Project</label>"
														   + "	<input name=website id=website maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Github Link</label>"
														   + "	<input name=githublink id=githublink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Bitcoin Talk Link</label>"
														   + "	<input name=btctalklink id=btctalklink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Facebook Link</label>"
														   + "	<input name=facebooklink id=facebooklink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Twitter Link</label>"
														   + "	<input name=twitterlink id=twitterlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Discod Link</label>"
														   + "	<input name=discordlink id=discordlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Telegram Link</label>"
														   + "	<input name=telegramlink id=telegramlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Description of your project</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What are the Coin Specs</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is Masternode Online Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is Masternode Colateral and Rewards</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Explorer Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />";														   
		}else if(selval == 3){
			document.getElementById("projectinfo").innerHTML = "<label class=control-label>What is the name of your Project</label>" 
														   + "	<input name=projname id=projname maxlength=200 type=text required=required class=form-control placeholder=Enter Project Name />" 
														   + "</div>" 
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Ticker for your Project</label>"
														   + "	<input name=ticker id=ticker maxlength=5 type=text required=required class=form-control placeholder=Enter Ticker />"
														   + "</div>" 	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the URL for your Project</label>"
														   + "	<input name=website id=website maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Github Link</label>"
														   + "	<input name=githublink id=githublink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Bitcoin Talk Link</label>"
														   + "	<input name=btctalklink id=btctalklink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Facebook Link</label>"
														   + "	<input name=facebooklink id=facebooklink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Twitter Link</label>"
														   + "	<input name=twitterlink id=twitterlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Discod Link</label>"
														   + "	<input name=discordlink id=discordlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Telegram Link</label>"
														   + "	<input name=telegramlink id=telegramlink maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Description of your project</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What are the Coin Specs</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is Masternode Online Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is Masternode Colateral and Rewards</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Explorer Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What Exchanges is the coin listed on</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the KYD Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Whitepaper Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Roadmap Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Launch Date</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the Pre-Sale Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />"	
														   + "<div class=form-group>"
														   + "	<label class=control-label>What is the ROI Calculator Link</label>"
														   + "	<input maxlength=200 type=text required=required class=form-control placeholder=Enter Website URL />";															   
		}
	}else{
		alert("bad");
	}
} 

			




