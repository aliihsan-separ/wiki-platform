<?php
$page = 'home';

require_once('database.php');
require_once('header.php');
if(isset($_POST) && isset($_POST['AlgoSel'])) {
	$filterme = $_POST['AlgoSel'];
}else{
	$filterme = "00";
}
?>
<div class="bg">
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <center><img src="images\banner.png" alt="" width="50%" height="30%"></center>'
                </div>
				<div class="col s2">
					<form role="form" id="filtercoin" name="filtercoin" action="<?php echo $_SERVER['PHP_SELF']  ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<select class="form-control form-control-lg" name="AlgoSel" id="AlgoSel" onchange='this.form.submit()'>
								<option value="" disabled="disabled" selected="selected">Filter by Algo ...</option>
<?php					
			echo "<option value=00>All Algo's</option>";
			$result = $mysqli->query("SELECT DISTINCT specsalgo FROM coins");
			if($result) 
			{
                while($row = $result->fetch_object()) 
                {						
					$specsalgoout = $row->specsalgo;
					if($specsalgoout == 0){ $selectalgo = "--------"; $selectalgonum = "00";}
					if($specsalgoout == 1){ $selectalgo = "aergo"; $selectalgonum = "1"; }
					if($specsalgoout == 2){ $selectalgo = "allium";  $selectalgonum = "2";}
					if($specsalgoout == 3){ $selectalgo = "bcd";  $selectalgonum = "3";}
					if($specsalgoout == 4){ $selectalgo = "bitcore";  $selectalgonum = "4";}
					if($specsalgoout == 5){ $selectalgo = "blake2s";  $selectalgonum = "5";}
					if($specsalgoout == 6){ $selectalgo = "blakecoin";  $selectalgonum = "6";}
					if($specsalgoout == 7){ $selectalgo = "c11";  $selectalgonum = "7";}
					if($specsalgoout == 8){ $selectalgo = "groestl";  $selectalgonum = "8";}
					if($specsalgoout == 9){ $selectalgo = "hex";  $selectalgonum = "9";}
					if($specsalgoout == 10){ $selectalgo = "hmq1725";  $selectalgonum = "10";}
					if($specsalgoout == 11){ $selectalgo = "keccak";  $selectalgonum = "11";}
					if($specsalgoout == 12){ $selectalgo = "keccakc";  $selectalgonum = "12";}
					if($specsalgoout == 13){ $selectalgo = "lbk3";  $selectalgonum = "13";}
					if($specsalgoout == 14){ $selectalgo = "lbry";  $selectalgonum = "14";}
					if($specsalgoout == 15){ $selectalgo = "lyra2v2";  $selectalgonum = "15";}
					if($specsalgoout == 16){ $selectalgo = "lyra2z";  $selectalgonum = "16";}
					if($specsalgoout == 17){ $selectalgo = "m7m";  $selectalgonum = "17";}
					if($specsalgoout == 18){ $selectalgo = "myr-gr";  $selectalgonum = "18";}
					if($specsalgoout == 19){ $selectalgo = "neoscrypt";  $selectalgonum = "19";}
					if($specsalgoout == 20){ $selectalgo = "nist5";  $selectalgonum = "20";}
					if($specsalgoout == 21){ $selectalgo = "phi";  $selectalgonum = "21";}
					if($specsalgoout == 22){ $selectalgo = "phi2";  $selectalgonum = "22";}
					if($specsalgoout == 23){ $selectalgo = "quark";  $selectalgonum = "23";}
					if($specsalgoout == 24){ $selectalgo = "qubit";  $selectalgonum = "24";}
					if($specsalgoout == 25){ $selectalgo = "scrypt";  $selectalgonum = "25";}
					if($specsalgoout == 26){ $selectalgo = "sib";  $selectalgonum = "26";}
					if($specsalgoout == 27){ $selectalgo = "skein";  $selectalgonum = "27";}
					if($specsalgoout == 28){ $selectalgo = "skunk";  $selectalgonum = "28";}
					if($specsalgoout == 29){ $selectalgo = "tribus";  $selectalgonum = "29";}
					if($specsalgoout == 30){ $selectalgo = "x11";  $selectalgonum = "30";}
					if($specsalgoout == 31){ $selectalgo = "x16r";  $selectalgonum = "31";}
					if($specsalgoout == 32){ $selectalgo = "x16s";  $selectalgonum = "32";}
					if($specsalgoout == 33){ $selectalgo = "x17";  $selectalgonum = "33";}
					if($specsalgoout == 34){ $selectalgo = "x22i";  $selectalgonum = "34";}
					if($specsalgoout == 35){ $selectalgo = "xevan";  $selectalgonum = "35";}
					if($specsalgoout == 36){ $selectalgo = "yescrypt";  $selectalgonum = "36";}
					if($specsalgoout == 37){ $selectalgo = "sha256";  $selectalgonum = "37";}
				
					echo "<option value=$selectalgonum>$selectalgo</option>";
				}
			}
?>			
							</select>
						</div>
					</form>
				</div>
				<div class="col s2"><button type="submit" name="coins" class="light-blue darken-4 btn btn-primary center-block fullwidth">Coins</button></div>
				<div class="col s2"><button type="submit" name="pools" class="light-blue darken-4 btn btn-primary center-block fullwidth">Pools</button></div>
				<div class="col s2"><button type="submit" name="pools" class="light-blue darken-4 btn btn-primary center-block fullwidth">Shared MN</button></div>
				<div class="col s2"></div>
				<div class="col s2"></div>
            </div>
            <div class="row" style="">
			
			<?php
			$i=0;
			if($filterme == "00"){
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name");
			}else{
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t1.specsalgo=$filterme");
			}
			if($result) 
			{
                while($row = $result->fetch_object()) 
                {
					$i++;
                    $symbol = $row->symbol;                   
                    $logo = $row->logo;                    
                    $url = $row->url;              
					$approvedp = $row->approvedp;
					$approvedf = $row->approvedf;
					if($approvedp == '1' && $approvedf == '1'){
						echo '<div class="col s12 m6 l2">';
						echo '    <div class="card-panel grey lighten-3">';
						echo '        <div class="ribbon featured"><span>FEATURED</span></div>';
						echo '        <center><img src="images\coins\\' . $logo . '" alt="" height="100" width="100"></center>';
						echo '        <div class="content">';
						echo '            <h5><center>' . $symbol . '</center></h5>';
						echo '                    <div class="row">';
						echo '						<form method="POST" action="details.php" id="sample_form' . $i . '">';
						echo '							<input type="hidden" value="' . $symbol . '" name="symbol" />';
						echo '						</form>';
						echo '                        <a href="javascript:void(0);" onclick="submitForm' . $i . '();" class="col s12 light-blue darken-4 btn">More Info</a>';
						echo '                    </div>';
						echo '                    <div class="row">';
						echo '                        <a href="' . $url . '" target="_blank" class="col s12 btn green lighten-1">Website</a>';
						echo '                    </div>';
						echo '        </div>';
						echo '    </div>';
						echo '</div>';
						echo "<script>function submitForm$i() {  document.getElementById('sample_form$i').submit();   return true;}</script>";
					}
				}
			}
			$i=0;

			if($filterme == "00"){
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '3'");
			}else{
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '3' and t1.specsalgo=$filterme");
			}			
			if($result) 
			{
                while($row = $result->fetch_object()) 
                {
					$i++;					
                    $symbol = $row->symbol;                   
                    $logo = $row->logo;                    
                    $url = $row->url;              
					$approvedp = $row->approvedp;
					$approvedf = $row->approvedf;
					if($approvedp == '1' && $approvedf == '0'){
						echo '<div class="col s12 m6 l2">';
						echo '    <div class="card-panel grey lighten-3">';
						echo '        <div class="ribbon gold"><span>GOLD</span></div>';
						echo '        <center><img src="images\coins\\' . $logo . '" alt="" height="100" width="100"></center>';
						echo '        <div class="content">';
						echo '            <h5><center>' . $symbol . '</center></h5>';
						echo '                    <div class="row">';
						echo '						<form method="POST" action="details.php" id="sample_forma' . $i . '">';
						echo '							<input type="hidden" value="' . $symbol . '" name="symbol" />';
						echo '						</form>';
						echo '                        <a href="javascript:void(0);" onclick="submitForma' . $i . '();" class="col s12 light-blue darken-4 btn">More Info</a>';
						echo '                    </div>';
						echo '                    <div class="row">';
						echo '                        <a href="' . $url . '" target="_blank" class="col s12 btn green lighten-1">Website</a>';
						echo '                    </div>';
						echo '        </div>';
						echo '    </div>';
						echo '</div>';
						echo "<script>function submitForma$i() {  document.getElementById('sample_forma$i').submit();   return true;}</script>";
					}
				}
			}
			$i=0;
			if($filterme == "00"){
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '2'");
			}else{
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '2' and t1.specsalgo=$filterme");
			}
			if($result) 
			{
                while($row = $result->fetch_object()) 
                {
					$i++;					
                    $symbol = $row->symbol;                   
                    $logo = $row->logo;                    
                    $url = $row->url;              
					$approvedp = $row->approvedp;
					$approvedf = $row->approvedf;
					if($approvedp == '1' && $approvedf == '0'){
						echo '<div class="col s12 m6 l2">';
						echo '    <div class="card-panel grey lighten-3">';
						echo '        <div class="ribbon silver"><span>SILVER</span></div>';
						echo '        <center><img src="images\coins\\' . $logo . '" alt="" height="100" width="100"></center>';
						echo '        <div class="content">';
						echo '            <h5><center>' . $symbol . '</center></h5>';
						echo '                    <div class="row">';
						echo '						<form method="POST" action="details.php" id="sample_formb' . $i . '">';
						echo '							<input type="hidden" value="' . $symbol . '" name="symbol" />';
						echo '						</form>';
						echo '                        <a href="javascript:void(0);" onclick="submitFormb' . $i . '();" class="col s12 light-blue darken-4 btn">More Info</a>';
						echo '                    </div>';
						echo '                    <div class="row">';
						echo '                        <a href="' . $url . '" target="_blank" class="col s12 btn green lighten-1">Website</a>';
						echo '                    </div>';
						echo '        </div>';
						echo '    </div>';
						echo '</div>';
						echo "<script>function submitFormb$i() {  document.getElementById('sample_formb$i').submit();   return true;}</script>";
					}
				}
			}
			$i=0;
			$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '1'");
			if($filterme == "00"){
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '1'");
			}else{
				$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '1' and t1.specsalgo=$filterme");
			}			
			if($result) 
			{
                while($row = $result->fetch_object()) 
                {
					$i++;					
                    $symbol = $row->symbol;                   
                    $logo = $row->logo;                    
                    $url = $row->url;              
					$approvedp = $row->approvedp;
					$approvedf = $row->approvedf;
					if($approvedp == '1' && $approvedf == '0'){
						echo '<div class="col s12 m6 l2">';
						echo '    <div class="card-panel grey lighten-3">';
						echo '        <div class="ribbon bronze"><span>BRONZE</span></div>';
						echo '        <center><img src="images\coins\\' . $logo . '" alt="" height="100" width="100"></center>';
						echo '        <div class="content">';
						echo '            <h5><center>' . $symbol . '</center></h5>';
						echo '                    <div class="row">';
						echo '						<form method="POST" action="details.php" id="sample_formc' . $i . '">';
						echo '							<input type="hidden" value="' . $symbol . '" name="symbol" />';
						echo '						</form>';
						echo '                        <a href="javascript:void(0);" onclick="submitFormc' . $i . '();" class="col s12 light-blue darken-4 btn">More Info</a>';
						echo '                    </div>';
						echo '                    <div class="row">';
						echo '                        <a href="' . $url . '" target="_blank" class="col s12 btn green lighten-1">Website</a>';
						echo '                    </div>';
						echo '        </div>';
						echo '    </div>';
						echo '</div>';
						echo "<script>function submitFormc$i() {  document.getElementById('sample_formc$i').submit();   return true;}</script>";
					}
				}
			}
			$i=0;			
			$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '0' ORDER BY RAND()");
			if($filterme == "00"){
			$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '0' ORDER BY RAND()");
			}else{
			$result = $mysqli->query("SELECT t1.symbol, t1.logo, t1.url, t2.approvedp, t2.package, t2.expire, t3.id, t3.approvedf, t3.end, t3.start FROM coins t1 inner join package t2 on t1.symbol = t2.name inner join featured t3 on t1.symbol = t3.name where t2.package = '0' and t1.specsalgo=$filterme ORDER BY RAND()");
			}			
			if($result) 
			{
                while($row = $result->fetch_object()) 
                {
					$i++;					
                    $symbol = $row->symbol;                   
                    $logo = $row->logo;                    
                    $url = $row->url;              
					$approvedp = $row->approvedp;
					$approvedf = $row->approvedf;
					if($approvedp == '1' && $approvedf == '0'){
						echo '<div class="col s12 m6 l2">';
						echo '    <div class="card-panel grey lighten-3">';
						echo '        <center><img src="images\coins\\' . $logo . '" alt="" height="100" width="100"></center>';
						echo '        <div class="content">';
						echo '            <h5><center>' . $symbol . '</center></h5>';
						echo '                    <div class="row">';
						echo '						<form method="POST" action="details.php" id="sample_formd' . $i . '">';
						echo '							<input type="hidden" value="' . $symbol . '" name="symbol" />';
						echo '						</form>';
						echo '                        <a href="javascript:void(0);" onclick="submitFormd' . $i . '();" class="col s12 light-blue darken-4 btn">More Info</a>';
						echo '                    </div>';
						echo '                    <div class="row">';
						echo '                        <a href="' . $url . '" target="_blank" class="col s12 btn green lighten-1">Website</a>';
						echo '                    </div>';
						echo '        </div>';
						echo '    </div>';
						echo '</div>';
						echo "<script>function submitFormd$i() {  document.getElementById('sample_formd$i').submit();   return true;}</script>";
					}
				}
			}
									
			?>	
				


            </div>
        </div>
    </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>

<?php require_once('footer.php'); ?>