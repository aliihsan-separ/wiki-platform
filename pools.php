<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
$page = 'pools';
require_once('database.php');
require_once('header.php');

function convertToReadableSize($size){
  $base = log($size) / log(1024);
  $suffix = array("", "KB", "MB", "GB", "TB");
  $f_base = floor($base);
  return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
}
if(isset($_POST) && isset($_POST['AlgoSel'])) {
	$filterme = $_POST['AlgoSel'];
}else{
	$filterme = "0";
}
?>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h4>!!!Find the best pool to mine you coins at!!!</h4>
            </div>	
			<div class="col s2">
				<form role="form" id="filtercoin" name="filtercoin" action="<?php echo $_SERVER['PHP_SELF']  ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<select class="form-control form-control-lg" name="AlgoSel" id="AlgoSel" onchange='this.form.submit()'>
							<option value="" disabled="disabled" selected="selected">Filter by Algo ...</option>
<?php					
		$result = $mysqli->query("SELECT DISTINCT specsalgo FROM coins ORDER BY specsalgo");
		if($result) 
		{
			while($row = $result->fetch_object()) 
			{						
				$specsalgoout = $row->specsalgo;
				if($specsalgoout == 0){ $selectalgo = "All Algo's"; $selectalgonum = "0"; }
				if($specsalgoout == 1){ $selectalgo = "aergo"; $selectalgonum = "1"; }
				if($specsalgoout == 2){ $selectalgo = "allium";  $selectalgonum = "2";}
				if($specsalgoout == 3){ $selectalgo = "bcd";  $selectalgonum = "3";}
				if($specsalgoout == 4){ $selectalgo = "bitcore";  $selectalgonum = "4";}
				if($specsalgoout == 5){ $selectalgo = "blake2s";  $selectalgonum = "5";}
				if($specsalgoout == 6){ $selectalgo = "blakecoin";  $selectalgonum = "6";}
				if($specsalgoout == 7){ $selectalgo = "c11";  $selectalgonum = "7";}
				if($specsalgoout == 8){ $selectalgo = "groestl";  $selectalgonum = "8";}
				if($specsalgoout == 9){ $selectalgo = "hex";  $selectalgonum = "9";}
				if($specsalgoout == 10){ $selectalgo = "hmq1725";  $selectalgonum = "10";}
				if($specsalgoout == 11){ $selectalgo = "keccak";  $selectalgonum = "11";}
				if($specsalgoout == 12){ $selectalgo = "keccakc";  $selectalgonum = "12";}
				if($specsalgoout == 13){ $selectalgo = "lbk3";  $selectalgonum = "13";}
				if($specsalgoout == 14){ $selectalgo = "lbry";  $selectalgonum = "14";}
				if($specsalgoout == 15){ $selectalgo = "lyra2v2";  $selectalgonum = "15";}
				if($specsalgoout == 16){ $selectalgo = "lyra2z";  $selectalgonum = "16";}
				if($specsalgoout == 17){ $selectalgo = "m7m";  $selectalgonum = "17";}
				if($specsalgoout == 18){ $selectalgo = "myr-gr";  $selectalgonum = "18";}
				if($specsalgoout == 19){ $selectalgo = "neoscrypt";  $selectalgonum = "19";}
				if($specsalgoout == 20){ $selectalgo = "nist5";  $selectalgonum = "20";}
				if($specsalgoout == 21){ $selectalgo = "phi";  $selectalgonum = "21";}
				if($specsalgoout == 22){ $selectalgo = "phi2";  $selectalgonum = "22";}
				if($specsalgoout == 23){ $selectalgo = "quark";  $selectalgonum = "23";}
				if($specsalgoout == 24){ $selectalgo = "qubit";  $selectalgonum = "24";}
				if($specsalgoout == 25){ $selectalgo = "scrypt";  $selectalgonum = "25";}
				if($specsalgoout == 26){ $selectalgo = "sib";  $selectalgonum = "26";}
				if($specsalgoout == 27){ $selectalgo = "skein";  $selectalgonum = "27";}
				if($specsalgoout == 28){ $selectalgo = "skunk";  $selectalgonum = "28";}
				if($specsalgoout == 29){ $selectalgo = "tribus";  $selectalgonum = "29";}
				if($specsalgoout == 30){ $selectalgo = "x11";  $selectalgonum = "30";}
				if($specsalgoout == 31){ $selectalgo = "x16r";  $selectalgonum = "31";}
				if($specsalgoout == 32){ $selectalgo = "x16s";  $selectalgonum = "32";}
				if($specsalgoout == 33){ $selectalgo = "x17";  $selectalgonum = "33";}
				if($specsalgoout == 34){ $selectalgo = "x22i";  $selectalgonum = "34";}
				if($specsalgoout == 35){ $selectalgo = "xevan";  $selectalgonum = "35";}
				if($specsalgoout == 36){ $selectalgo = "yescrypt";  $selectalgonum = "36";}
				if($specsalgoout == 37){ $selectalgo = "sha256";  $selectalgonum = "37";}
			
				echo "<option value=$selectalgonum>$selectalgo</option>";
			}
		}
?>			
						</select>
					</div>
				</form>
			</div>
			<div class="col s2">
				<form role="form" id="filtercoin" name="filtercoin" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<select class="form-control form-control-lg" name="AlgoSel" id="AlgoSel" onchange='this.form.submit()'>
							<option value="" disabled="disabled" selected="selected">Sort by ...</option>
							<option value=0>Alphabet</option>
							<option value=0>Block Height</option>
							<option value=0>Network HashRate</option>
							<option value=0>Difficulty</option>
						</select>
					</div>
				</form>
			</div>
			<div class="col s2">
				<button type="submit" name="submit" class="light-blue darken-4 btn btn-primary center-block fullwidth">Submit</button>
			</div>
<?php
	$date = date("Y-m-d");
	$result = $mysqli->query("SELECT * FROM pools where expire >= '".$date."'");
	if($result) 
	{
		while($row = $result->fetch_object()) 
		{
			$id = $row->ID;
			$url = $row->poolurl;
			$pool = $row->poolname;			
			echo "<div class='col s12'>";
				echo "<ul class='collapsible'>";
					echo "<li class='active'>";
						echo "<div class='collapsible-header'>".$pool."</div>";
						echo "<div class='collapsible-body'>";
							echo "<table id='example' class='display dataTable no-footer' style='width:100%'>";
								echo "<thead>";
									echo "<tr>";
										echo "<th>Pool</th>";
										echo "<th>Block Height</th>";
										echo "<th>Workers</th>";
										echo "<th>Blocks in 24h</th>";
										echo "<th>Last Block</th>";
										echo "<th>Pool Hashrate</th>";
									echo "</tr>";
								echo "</thead>";
								echo "<tbody>";

								$result0 = $mysqli->query("SELECT MAX(blockheight) AS bh, blockin24, cryptoname, hashrate, lastblock, poolid, workers FROM `pooldata` where poolid = '" . $id . "' GROUP BY cryptoname");

								if($result0) 
								{
									while($row0 = $result0->fetch_object()) 
									{
										$name = $row0->cryptoname;
										$height = $row0->bh;
										$workers = $row0->workers;	
										$blockin24 = $row0->blockin24;	
										$lastblock = $row0->lastblock;	
										$hashrate = $row0->hashrate;	

										echo "<tr><td>" . $name ."</td><td>" . $height ."</td><td>" . $workers ."</td><td>" . $blockin24 ."</td><td>" . $lastblock ."</td><td>" . $hashrate ."</td><td>";
									}
								}

															
								echo "</tbody>";
							echo "</table>";
						echo "</div>";
					echo "</li>";
				echo "</ul>";
			echo "</div>";
		}
	}
?>
		</div>		
	</div>
</div>
<?php require_once('footer.php'); ?>