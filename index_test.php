<?php
$page = 'home';

require_once('database.php');
require_once('header.php');
if(isset($_POST) && isset($_POST['AlgoSel'])) {
	$filterme = $_POST['AlgoSel'];
}else{
	$filterme = "00";
}
?>
<div class="main" style="margin-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="col s12 m6">
                <div class="card-panel">
                    <h4>Login</h4>
                    <p>Sign in with your existing password.</p>

                    <form method="post" id="login">
                        <label for="seed">Password</label>
                        <input id="seed" name="seed" type="text" class="browser-default">

                        <p class="center-align"><input type="submit" href="#" class="light-blue darken-4 btn" value="Enter Account" /></p>
                    </form>
                </div>
            </div>
            <div class="col s12 m6">
                <div class="card-panel">
                    <h4>Register</h4>
                    <p>Create a new password with your email.</p>

                    <form id="register">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="browser-default">
                        <p class="center-align"><input type="submit" href="#" class="light-blue darken-4 btn" value="Create Account" /></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="regModal" class="modal">
    <div class="modal-content">
        <h4>Account created</h4>
        <p>Your account password is:</p>
        <pre id="seedRes" style="font-weight: bold;"></pre>
        <p>Please save this as there is no way to recover the password after this point.</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
</div>
<?php require_once('footer.php'); ?>