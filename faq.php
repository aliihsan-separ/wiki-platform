<?php
$page = 'faq';
require_once('database.php');
require_once('header.php');
?>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h1>FAQ</h1>
                <p>We know you have a lot of questions and we have compiled these answers based on what the community asked us. For any unanswered question, you can find a member of our team online on
                    <a href="#" target="_blank" rel="nofollow noopener">Discord</a>, please be sure to only talk to members highlighted as yellow or blue to avoid getting scammed.</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <ul class="collapsible">
                    <li class="active">
                        <div class="collapsible-header">What is WikiCoin ?</div>
                        <div class="collapsible-body">
                            WikiCoin is a cryptocurrency that can be used on the AltcoinWiki platform where you can advertise your own cryptocurrency aswell as find all the information you want about any other altcoin.
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header">WikiCoin for Cryptocurrency Developers ?</div>
                        <div class="collapsible-body">
                            Developers of cryptocurrency can use the Altcoinwiki to list there cryptocurrency with all the nessacary information about there cryptocurrency depending on there subscription.
                        </div>
                    </li>
					<li>
                        <div class="collapsible-header">WikiCoin for investors and Hodlers.</div>
                        <div class="collapsible-body">
                            AltcoinWiki will be a big platform where you can find all the information about any recently released coin or a coin that has been existing for a long time.<br> No need to google information about a coin and spend hours on bitcointalk. <br>All the information you need in 1 place. The more coins get listed and the more users that use the platform the higher the demand of listing gets.
                        </div>
                    </li>
					<li>
                        <div class="collapsible-header">Why the AltcoinWiki platform ?</div>
                        <div class="collapsible-body">
                           Alot of people have issues finding information about a new or old altcoin they might wanna invest in or mine. You have to spend hours of searching on google, bitcointalk etc to find the altcoins Exchange, Explorer, Twitter, Github, Discord, Specifications, Roadmap, Whitepaper, Roi claculator, Mining calculator, Masternode Guide and more.<br> With ALtcoinWiki you can just check the list or use the search function and click on the more info button and all this info will be available based on the paid subscription.

                        </div>
                    </li>
					<li>
                        <div class="collapsible-header">Is the AltcoinWiki Platform working and available on launch ?</div>
                        <div class="collapsible-body">
                           Yes the AltcoinWiki platform is fully operational and working as soon as we launch.<br> You will be able to register purchase a subscription add your cryptocurrency's info and more. <br>Also the free listing option will be there at launch. Simply Register at www.altcoinwiki.io and request the listing / subscription you want.
                        </div>
                    </li>
                   
					 
					 
					 
					 <li>
                        <div class="collapsible-header">What does WikiCoin do with the listing fees ?</div>
                        <div class="collapsible-body">
                          WikiCoin will use the listing fees paid on the AltcoinWiki to improve our services, expand the platform, update and advertise our platform.
                        </div>
                    </li>
					<li>
                        <div class="collapsible-header">What does WikiCoin Exchange offer ?</div>
                        <div class="collapsible-body">
                          WikiCoin exchange offers you the opportunity to list your cryptocurrency on our exchange paying per month instead of paying a high amount and having the risk of being delisted. <br >This way you can try out our exchange for a decent price . WikiCoin also gives new cryptocurrency  a chance to list on our exchange at an affordable price.
                        </div>
                    </li>
					 <li>
                        <div class="collapsible-header">When does WikiCoin Exchange go live ?</div>
                        <div class="collapsible-body">
                          The launch for the WikiCoin exchange is planned for Q1 - 2019 and will be intergrated into the AltcoinWiki Platform.
                        </div>
                    </li>
					 <li>
                        <div class="collapsible-header">What are the Tier Prices ?</div>
                        <div class="collapsible-body">
                            <table class="table table-striped">
								<thead>
									<th colspan=2>Altcoin Wiki uses the following TIER prices based on the presale price of Wikicoin.</th>
								</thead>
								<tbody>
									<tr>
										<td>Free Lifetime = Free.</td>
									</tr>
									<tr>
										<td>Bronze 1 month subscription = 90 WikiCoins</td>
									</tr>
									<tr>										
										<td>Silver 1 month subscription = 137 WikiCoins</td>
									</tr>
									<tr>										
										<td>Gold 1 month subscription = 182 WikiCoins</td>
									</tr>
									<tr>										
										<td>Exchange 1 month subscription = 2270 WikiCoins</td>
									</tr>
									<tr>										
										<td>Featured 3 days subscription top 5 = 100 WikiCoins</td>
									</tr>
									<tr>										
										<td>Featured 3 days subscription top 5 - 10 = 73 WikiCoins</td>
									</tr>
									<tr>										
										<td>Featured 3 days subscription top 10 - 20 = 37 WikiCoins</td>
									</tr>
									<tr>										
										<td>Banner advertisment on AltcoinWiki 1 week subscription = 137 WikiCoins</td>
									</tr>
									<th colspan=2>After Wikicoin gets listed on exchange the prices will be calculated from dollar to Wikicoin value using api calls on the AltcoinWiki.								</th>
								</tbody>
							</table>


                        </div>
                    </li>
					<li>
                        <div class="collapsible-header">What are the fees for lising on the WikiCoin Exchange ?</div>
                        <div class="collapsible-body">
                          The fee for listing on the WikiCoin exchange are $499 per month = 2270 WikiCoins based on presale prices.
                        </div>
                    </li>
				                </ul>
            </div>
        </div>
    </div>
</div>

<?php require_once('footer.php'); ?>