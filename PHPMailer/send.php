<?php
/**
 * Created by GGDevs.
 * User: Sansei
 * Date: 25.01.2019
 * Time: 00:58
 */

include "class.phpmailer.php";

if(isset($_POST['mail'])) {

    $coin_name = $_POST['coin_name'];
    $ticker = $_POST['ticker'];
    $algos = ["aergo", "allium", "bcd", "bitcore", "blake2s", "blakecoin", "c11", "groestl", "hex", "hmq1725", "keccak", "keccakc", "lbk3", "lbry", "lyra2v2", "lyra2z", "m7m", "myr-gr", "neoscrypt", "nist5", "phi", "phi2", "quark", "qubit", "scrypt", "sib", "skein", "skunk", "tribus", "x11", "x16r", "x16s", "x17", "x22i", "xevan", "yescrypt", "sha256"];
    $algo = $algos[$_POST['coin_name']];

    $codebase = $_POST['codebase'];
    $explorer = $_POST['explorer'];
    $website = $_POST['website'];
    $btctalk = $_POST['btctalk'];
    $github = $_POST['github'];
    $discord = $_POST['discord'];
    $twitter = $_POST['twitter'];
    $supply = $_POST['supply'];
    $email = $_POST['email'];
    $discord_name = $_POST['discord_name'];
    $txid = $_POST['txid'];


    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPAuth = true;
    $mail->Host = 'mail.privateemail.com';
    $mail->Port = 465;
    $mail->SMTPSecure = 'ssl';
    $mail->Username = 'support@wiki.exchange';
    $mail->Password = ':xq1NCO7rhk*[';
    $mail->SetFrom($mail->Username, 'List On Exhange Form');
    $mail->AddAddress('admin@wiki.exchange', 'Wiki Admin');
    $mail->CharSet = 'UTF-8';
    $mail->Subject = 'A New Request For Listed On Exchange From AltCoinWIKI';
    $content = '<div style="background: #eee; padding: 10px; font-size: 14px"> 
                    <div class="row">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="coin_name"> Coin Name : '.$coin_name.' </label>
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="ticker"> Ticker : '.$ticker.' </label>
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="algo"> Algo : '.$algo.' </label>
                                       
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="codebase"> Codebase ( for example : Pivx, Dash etc ) :  '.$codebase.'</label>
                                       
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="explorer"> Block Explorer : '.$explorer.'</label>
                                       
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="website"> Official Website : '.$website.'</label>
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="btctalk"> Bitcointalk ann : '.$btctalk.'</label>
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="github"> Github source : '.$github.'</label>
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="discord"> Discord server : '.$discord.' </label>
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="twitter"> Twitter : '.$twitter.'</label>
                                       
                                    </div>
                                    
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="supply"> Maximum supply : '.$supply.'</label>
                                        
                                    </div>

                                    <div class="form-group col-md-6 col-xs-12">
                                        <label for="email"> Email address : '.$email.' </label>
                                        
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <label for="discord_name"> Your contact username on discord including the # number : '.$discord_name.'</label>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                </div>';
    $mail->AddAttachment($_FILES['myfile']['tmp_name'],$_FILES['myfile']['name']);
    $mail->MsgHTML($content);

    if($mail->Send()) {
        header('Location: ../dashboard.php');
    } else {

        echo $mail->ErrorInfo;
        exit;
    }
}
