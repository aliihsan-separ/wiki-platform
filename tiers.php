<?php
$page = 'TOS';
require_once('database.php');
require_once('header.php');
?>

<div class="main">
    <div class="container">
        <div class="row flex">

            <div class="col m4"></div>
			<div class="col m2"><center><img src="images\free_tier.png" alt="Tiers" width="90%" height="90%"></center></div>
			<div class="col m2"><center><img src="images\bronze_tier.png" alt="Tiers" width="90%" height="90%"></center></div>
			<div class="col m2"><center><img src="images\silver_tier.png" alt="Tiers" width="90%" height="90%"></center></div>
			<div class="col m2"><center><img src="images\gold_tier.png" alt="Tiers" width="90%" height="90%"></center></div>

            <div class="col s4 grey lighten-4"><font size="5">Name & Logo</font></div>
			<div class="col s2 orange lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col s2 red lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col s2 blue lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col s2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-2"><font size="5">Website URL</font></div>
			<div class="col m2 orange lighten-4"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 red lighten-4"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 blue lighten-4"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 yellow lighten-4"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-4"><font size="5">Github Link</font></div>
			<div class="col m2 orange lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 blue lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-2"><font size="5">BitcoinTalk Link</font></div>
			<div class="col m2 orange lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-4"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 blue lighten-4"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 yellow lighten-4"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-4"><font size="5">Discord & Telegram & Facebook</font></div>
			<div class="col m2 orange lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 blue lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-2"><font size="5">Description</font></div>
			<div class="col m2 orange lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-4"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 yellow lighten-4"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-4"><font size="5">Coin Specs</font></div>
			<div class="col m2 orange lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-5"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-2"><font size="5">Explorer Link</font></div>
			<div class="col m2 orange lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-4"><center><img src="images\checkmark.png"></center></div>
			<div class="col m2 yellow lighten-4"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-4"><font size="5">Exchanges</font></div>
			<div class="col m2 orange lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-2"><font size="5">KYD</font></div>
			<div class="col m2 orange lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 yellow lighten-4"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-4"><font size="5">Whitepaper</font></div>
			<div class="col m2 orange lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-2"><font size="5">Roadmap</font></div>
			<div class="col m2 orange lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 yellow lighten-4"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-4"><font size="5">Launch Date</font></div>
			<div class="col m2 orange lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-2"><font size="5">Presale Link</font></div>
			<div class="col m2 orange lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-4"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 yellow lighten-4"><center><img src="images\checkmark.png"></center></div>

            <div class="col m4 grey lighten-4"><font size="5">ROI Calculator</font></div>
			<div class="col m2 orange lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 red lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 blue lighten-5"><center><img src="images\crossmark.png"></center></div>
			<div class="col m2 yellow lighten-5"><center><img src="images\checkmark.png"></center></div>
        </div>		
    </div>
</div>

<?php require_once('footer.php'); ?>